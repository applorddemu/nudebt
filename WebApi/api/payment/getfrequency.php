<?php
header("Content-type: text/xml");
echo '<ArrayOfKeyValuePairOfintstring xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/System.Collections.Generic">
    <KeyValuePairOfintstring>
        <key>1</key>
        <value>Once-Off</value>
    </KeyValuePairOfintstring>
    <KeyValuePairOfintstring>
        <key>2</key>
        <value>Monthly</value>
    </KeyValuePairOfintstring>
    <KeyValuePairOfintstring>
        <key>3</key>
        <value>Weekly</value>
    </KeyValuePairOfintstring>
    <KeyValuePairOfintstring>
        <key>4</key>
        <value>FortNightly</value>
    </KeyValuePairOfintstring>
</ArrayOfKeyValuePairOfintstring>';
?>