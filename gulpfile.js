﻿/// <reference path="client/app/app.module.js" />
/// <reference path="client/app/app.module.js" />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp'),
    glob = require('glob'),
    globAll = require('glob-all'),
    es = require('event-stream');
//var sourcemaps = require('gulp-sourcemaps');
//var cleanCSS = require('gulp-clean-css');

var concat = require('gulp-concat');
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');

var baseFilePath = './';
var dest = 'build';

gulp.task('default', ['jsminify']);

gulp.task('jsminify', function () {
    var appJsFilePath = baseFilePath + '**/*.js';

    displayPathFileCount('jsminify', appJsFilePath);

    gulp.src([appJsFilePath])
    .pipe(uglify())
    .pipe(gulp.dest(dest));
});

gulp.task('css-minify', function () {
    var appCsssFilePath = baseFilePath + '**/*.css';

    //displayPathFileCount('css-minify', appCsssFilePath);

    gulp.task('minify-css', function() {
        return gulp.src(appCsssFilePath)
            //.pipe(sourcemaps.init())
            .pipe(cleanCSS({ compatibility: 'ie8' }))
            //.pipe(sourcemaps.write())
            .pipe(gulp.dest(dest));
    });
});

function displayPathFileCount(taskname,source)
{
    var files = globAll.sync(source);
    console.log('total files in ' + taskname + ' -> ' + files.length);
}