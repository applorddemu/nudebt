﻿(function () {
    'use strict';

    angular
        .module('app.layout')
         .directive('numeric', function () {
                   return {
                       require: 'ngModel',
                       restrict: 'A',
                       scope: {
                           max: '=',
                           min: '='
                       },
                       link: function (scope, element, attr, ctrl) {
                           function inputValue(val) {
                               if (val) {
                                   var digits = val.replace(/[^0-9]/g, '');

                                   if (digits !== val) {
                                       ctrl.$setViewValue(digits);
                                       ctrl.$render();
                                   }
                                   return digits;
                               }
                               //else if (val.trim() == '')
                               //    return true;
                               return undefined;
                           }
                           ctrl.$parsers.push(inputValue);
                       }
                   };
               })
        .directive('alphanumeric', function () {
            return {
                restrict: 'A',
                link: function (scope, elm, attrs, ctrl) {
                    elm.on('keydown', function (event) {
                        var $input = $(this);
                        var value = $input.val();
                        value = value.replace(/[^a-zA-Z0-9]/g, '')
                        $input.val(value);
                        var keycode = (event.which) ? event.which : event.keyCode
                        if (keycode == 64 || keycode == 16) {
                            return false;//KEYCODE: CAPS & SMALL A-Z = 65-90, NORMAL 0-9 = 48-57, NUMLOCK(0-9) = 96-105
                        } else if ((keycode >= 48 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 96 && keycode <= 105) || (keycode >= 16 && keycode <= 20) || (keycode >= 112 && keycode <= 123) || keycode == 13) {
                            return true;
                        } else if ([8, 9, 13, 27, 37, 38, 39, 40, 46].indexOf(keycode) > -1) {
                            return true;
                        } else {
                            event.preventDefault();
                            return false;
                        }
                    });
                }
            }

        })
     .directive('zeroPrefix', function () {
         return {
             restrict: 'A',
             require: 'ngModel',
             link: function (scope, element, attrs, controller) {
                 function zeroPrefix(value) {
                     if (value && value.indexOf(0) !== 0 && value.length!==0) {
                         value = value.substring(0, 9)
                         controller.$setViewValue('0' + value);
                         controller.$render();
                         return '0' + value;
                     }
                     else if (value && value.length !== 0) {
                         value = value.substring(0, 10)
                         return value;
                     }
                 }
                 controller.$formatters.push(zeroPrefix);
                 controller.$parsers.splice(0, 0, zeroPrefix);
             }
         };
     })
    .directive('alphabet', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {

                    if (inputValue == undefined) return ''
                    var transformedInput = inputValue.replace(/[^a-zA-Z ]/g, '');
                    if (transformedInput != inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }

                    return transformedInput;
                });
            }
        };
    }) 
}());

