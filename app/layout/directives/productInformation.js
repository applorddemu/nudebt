﻿
(function () {
    'use strict';

    angular
        .module('app.layout')
        .directive('productInformation', function () {
            return {
                restrict: 'E',
                compile: function (element, attrs) {
                    var htmlText = '<div class="row">' +
                                     '<div class="col-md-12 italic">' +
                                       '<div class="col-md-12 alert alert-info product-class"> <div class="col-md-12">&nbsp;PRODUCT :<value class="italic"> {{vm.productName}}</value>' +
                                           //'<value class="italic"> {{vm.productId}}</value>' + ',' +
                                           //'<value class="italic"> {{vm.productSpeed}}</value></div></i>' +
                                        '</div>' +
                                      '</div>' +
                                     '</div>';
                    element.replaceWith(htmlText);
                }
            }; 
        });
}());

