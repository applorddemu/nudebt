﻿(function () {
    'use strict';

    angular
        .module('app.layout')
        .directive('pageMenu', ['$linq', 'EnumService', '$compile', function ($linq, EnumService, $compile) {
            var navigationType = EnumService.NavigationType.URLNavigation; // Default Navigation Type

            var userPages = [];
            var menuLevel = 0;
            var subMenuContent = '';

            function generateDynamicMenu($scope,elemenent) {
                var sideNavUL = $(elemenent).find('ul')[0];

                if (sideNavUL != null && userPages != null) {
                    generateMainMenu(null, sideNavUL);
                    $(".menu-dropdown").hover(function () {
                        $(this).addClass("open");
                    }, function () {
                        $(this).removeClass("open");
                    }
                   );
                }

                $compile(elemenent.contents())($scope);
            }

            function generateMainMenu(parentPageId, parentNode) {

                var pageList =orderByOrderNumber(getpageListByParentId(parentPageId));

                var html = '';
                angular.forEach(pageList, function (pageItem, index) {
                    menuLevel = 0;
                    getMenuLevelByPageId(pageItem.id);

                    //Master Pages
                    html += menuLevel > 1 ? '<li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">' : '<li class="menu-dropdown mega-menu-dropdown mega-menu-full">';

                    if (navigationType == EnumService.NavigationType.URLNavigation) {
                        html += '<a href="' + pageItem.pageURL + '"> <i class="' + pageItem.iconClass + '"></i> '+ pageItem.pageCaption +'</a>';
                    }
                    else
                    {
                        html += '<a href="#" ui-sref="' + pageItem.pageNavigationState + '"> <i class="' + pageItem.iconClass + '"></i> ' + pageItem.pageCaption + '</a>';
                    }

                    var childPageList = orderByOrderNumber(getpageListByParentId(pageItem.id));

                    if (childPageList) {
                        var hasChildPageExist = childPageList.length > 0;

                        //Level 1 - Menu Has Only one Level
                        if (menuLevel == 1) {
                            if (hasChildPageExist) {
                                html += '<ul class="dropdown-menu" style="min-width: "><li><div class="mega-menu-content">';
                                angular.forEach(childPageList, function (childPageItem, childPageIndex) {
                                    var colIndex = childPageIndex + 1;

                                    if (colIndex == 1 || colIndex % 5 == 0) // Create Row for each 4 columns
                                        html += '<div class="row">';

                                    //Container - Start
                                    html += '<div class="col-md-3">';

                                    //UL
                                    html += '<ul class="mega-menu-submenu"><li>';
                                    if (navigationType == EnumService.NavigationType.URLNavigation) {
                                        html += '<a href="' + childPageItem.pageURL + '"><i class="' + childPageItem.iconClass + '"></i> ' + childPageItem.pageCaption + '</a>';
                                    }
                                    else
                                    {
                                        html += '<a ui-sref="' + childPageItem.pageNavigationState + '"><i class="' + childPageItem.iconClass + '"></i> ' + childPageItem.pageCaption + '</a>';
                                    }
                                    html += '</li></ul>';
                                    html += '</div>';

                                    if (colIndex % 4 == 0)
                                        html += '</div>';
                                    //Container -End
                                });

                                html += '</div></li></ul>';
                                //$(parentNode).html(html);
                            }
                        }

                            //Menu Has More then One Level
                        else if (menuLevel > 1) {
                            if (hasChildPageExist) {
                                html += '<ul class="dropdown-menu pull-left">';
                                angular.forEach(childPageList, function (childPageItem, childPageIndex) {

                                    subMenuContent = angular.copy(html);
                                    generateSubMenu(childPageItem);

                                    html = angular.copy(subMenuContent);
                                });

                                html += '</ul>';

                                //$(parentNode).html(html);
                            }
                        }
                    }
                    $(parentNode).html(html);

                });

            }

            function generateSubMenu(childPageItem) {

                var hasChildExist = hasChild(childPageItem.id);

                subMenuContent += hasChildExist ? '<li aria-haspopup="true" class="dropdown-submenu ">' : '<li aria-haspopup="true">';
                if (navigationType == EnumService.NavigationType.URLNavigation) {
                    subMenuContent += '<a href="' + childPageItem.pageURL + ';" class="nav-link nav-toggle ">';
                }
                else
                {
                    subMenuContent += '<a ui-sref="' + childPageItem.pageNavigationState + '" class="nav-link nav-toggle ">';
                }
                subMenuContent += '<i class="' + childPageItem.iconClass + '"></i> ' + childPageItem.pageCaption + '';
                subMenuContent += ' <span class="arrow"></span></a>';


                var pageList = orderByOrderNumber(getpageListByParentId(childPageItem.id));

                if (hasChildExist) {
                    if (pageList.length > 0) {

                        subMenuContent += '<ul class="dropdown-menu">';
                        angular.forEach(pageList, function (pageItem, pageIndex) {
                            generateSubMenu(pageItem);
                        });

                        subMenuContent += '</ul>';
                    }
                }
                subMenuContent += '</li>';
            }

            function orderByOrderNumber(pageList)
            {
                return $linq.Enumerable().From(pageList).OrderBy(function (x) { return x.orderNo; }).ToArray();
            }

            function isMaster(page) {
                return page.parentPageId == null;
            }
            function hasChild(pageId) {
                var hasChild = false;
                var pageList = getpageListByParentId(pageId);
                if (pageList)
                    hasChild = pageList.length > 0

                return hasChild;
            }
            function getpageListByParentId(parentId) {
                return userPages.filter(function (node) { return (node.parentPageId == parentId); });
            }

            function getMenuLevelByPageId(pageId) {
                var childList = getpageListByParentId(pageId);

                if (childList) {
                    if (childList.length > 0) {
                        menuLevel++;
                        angular.forEach(childList, function (pageItem, pageIndex) {
                            getMenuLevelByPageId(pageItem.id);
                        });
                    }
                }
            }

            function link($scope, element, attrs) {
                $scope.$watch('userPages', function (newValue, oldValue) {
                    if (newValue) {
                        if (newValue.length > 0) {
                            userPages = angular.copy(newValue);
                            generateDynamicMenu($scope,element);
                        }
                    }
                }, true);

                if ($scope.navigationType) {
                    navigationType = $scope.navigationType;
                }
            }

            return {
                restrict: 'AE',
                transclude: false,
                replace: true,
                template: '<div class="hor-menu"><ul class="nav navbar-nav"> </ul></div>',
                link: link,
                scope: {
                    userPages: '=',
                    navigationType:'=?'
                },
            };
        }]);
}());



