﻿(function () {
    'use strict';

    angular
        .module('app.layout').directive('serverValidate', ['$http', function ($http) {
            return {
                require: 'ngModel',
                restrict: 'A',
                link: function (scope, ele, attrs, c) {
                   
                    //console.log('wiring up ' + attrs.ngModel + ' to controller ' + c.$name);
                    scope.$watch('modelState', function () {
                        if (scope.modelState == null) return;
                        var modelStateKey = attrs.name;// attrs.serverValidate || attrs.ngModel;
                        modelStateKey = modelStateKey.replace('$index', scope.$index);

                        //if (modelStateKey.endsWith("Country")) {

                        //    modelStateKey = modelStateKey.substring(modelStateKey.indexOf(".") + 1, modelStateKey.length) + 'Id';
                        //}
                        //else
                            modelStateKey = modelStateKey.substring(modelStateKey.indexOf(".") + 1, modelStateKey.length);
                        var errormodel = null;

                        for (var i = 0; i < scope.modelState.length; i++) {
                            var propertyName = scope.modelState[i].PropertyName;

                            var ind = propertyName.indexOf('.');

                            if (ind > -1) {
                                propertyName = propertyName.substring(propertyName.indexOf(".") + 1, propertyName.length);
                            }

                            if (propertyName == modelStateKey) {
                                errormodel = scope.modelState[i];
                                break;
                            }
                        }
                        c.$setValidity('server', true);
                        if (errormodel != null) {
                            c.$setValidity('server', false);
                            c.$error.server = { "errorMessage": errormodel.ErrorMessage };

                        } else {
                            c.$setValidity('server', true);
                        }


                        ele.bind('focus', function (evt) {
                            c.$setValidity('server', true);
                        }).bind('blur', function (evt) {
                            c.$setValidity('server', true);
                        });
                    });

                    scope.$watch(attrs.ngModel, function (oldValue, newValue) {
                        if (oldValue != newValue) {
                            c.$setValidity('server', true);
                            c.$error.server = '';
                        }
                    });
                }
            };
        }]);
}());
