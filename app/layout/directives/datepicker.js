﻿(function () {
    'use strict';

    angular
        .module('app.layout')
        .directive('formdatepicker', ['dateService', '$compile', function (dateService, $compile) {
            return {
                restrict: 'E',
                scope: {
                    ngModel: '=',
                    dateOptions: '=',
                    opened: '=',
                    isDisabled: '=',
                    isRequired: '=',
                    elementName: '@',
                    formName: '=',
                },
                link: function ($scope, element, attrs) {
                    $scope.isValid = !$scope.isRequired;
                    $scope.open = function (event) {
                        event.preventDefault();
                        event.stopPropagation();
                        if ($scope.opened) {
                            $scope.opened = false;
                        }
                        else
                            $scope.opened = true;
                    };
                    $scope.clear = function () {
                        $scope.ngModel = null;
                    };
                    $scope.$watch('ngModel', function (nVal) {
                        $scope.validationErrorList = [];
                        if ($scope.formName) {
                            if ($scope.formName[$scope.elementName]) {
                                $scope.validationErrorList = $scope.formName[$scope.elementName].$error.server;
                            }
                        }
                        $scope.isValid = $scope.validationErrorList.length == 0 && $scope.isRequired ? nVal != undefined : true;
                    });

                    var template = '<p class="input-group">' +

                            '<input name="elementName" type="text" ng-class="{ \'form-control\': true, \'datepicker-has-error\': !isValid }" datepicker-popup="dd MMM yyyy" ng-model="ngModel" is-open="opened" min="minDate" max="maxDate" placeholder="DD MMM YYYY e.g. 01 Jan 2015"' +
                                'datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="isRequired" close-text="Close" ng-disabled="isDisabled" />' +
                            '<span class="input-group-btn">' +
                            '<button type="button" class="btn btn-primary" ng-click="open($event)" ng-disabled="isDisabled"><i class="fa fa-calendar-o"></i></button>' +
                            '</span>' +
                            '</p>'

                    element.html(template);
                    $compile(element.contents())($scope);
                },
                //template: '<p class="input-group">' +

                //            '<input name="elementName" type="text" class="form-control" datepicker-popup="dd MMM yyyy" ng-model="ngModel" is-open="opened" min="minDate" max="maxDate" placeholder="DD MMM YYYY e.g. 01 Jan 2015"' +
                //                'datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="isRequired" close-text="Close" ng-disabled="isDisabled" />' +
                //            '<span class="input-group-btn">' +
                //            '<button type="button" class="btn btn-primary" ng-click="open($event)" ng-disabled="isDisabled"><i class="fa fa-calendar-o"></i></button>' +
                //            '</span>' +
                //            '</p>'
            };
        }])
    //    .directive('numeric', function () {
    //    return {
    //        require: 'ngModel',
    //        restrict: 'A',
    //        scope: {
    //            max: '=',
    //            min: '='
    //        },
    //        link: function (scope, element, attr, ctrl) {
    //            function inputValue(val) {
    //                if (val) {
    //                    var digits = val.replace(/[^0-9]/g, '');

    //                    if (digits !== val) {
    //                        ctrl.$setViewValue(digits);
    //                        ctrl.$render();
    //                    }
    //                    return parseInt(digits, 10);
    //                }
    //                //else if (val.trim() == '')
    //                //    return true;
    //                return undefined;
    //            }
    //            ctrl.$parsers.push(inputValue);
    //        }
    //    };
    //   })
    //    .directive('alphanumeric', function () {
    //        return {
    //            restrict: 'A',
    //            link: function (scope, elm, attrs, ctrl) {
    //                elm.on('keydown', function (event) {
    //                    var $input = $(this);
    //                    var value = $input.val();
    //                    value = value.replace(/[^a-zA-Z0-9]/g, '')
    //                    $input.val(value);
    //                    var keycode = (event.which) ? event.which : event.keyCode
    //                    if (keycode == 64 || keycode == 16) {
    //                        return false;//KEYCODE: CAPS & SMALL A-Z = 65-90, NORMAL 0-9 = 48-57, NUMLOCK(0-9) = 96-105
    //                    } else if ((keycode >= 48 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 96 && keycode <= 105) || (keycode >= 16 && keycode <= 20) || (keycode >= 112 && keycode <= 123) || keycode == 13) {
    //                        return true;
    //                    } else if ([8, 9, 13, 27, 37, 38, 39, 40, 46].indexOf(keycode) > -1) {
    //                        return true;
    //                    } else {
    //                        event.preventDefault();
    //                        return false;
    //                    }
    //                });
    //            }
    //        }
          
    //    })
    //    .directive('passwordStrength', [
    //function () {
    //    return {
    //        require: 'ngModel',
    //        restrict: 'E',
    //        scope: {
    //            password: '=ngModel'
    //        },

    //        link: function (scope, elem, attrs, ctrl) {
    //            scope.$watch('password', function (newVal) {
    //                scope.strength = isSatisfied(newVal && newVal.length >= 8) +
    //                  isSatisfied(newVal && /[A-Z]/.test(newVal)) +                  
    //                  isSatisfied(newVal && /(?=.*\W)/.test(newVal)) +
    //                  isSatisfied(newVal && /\d/.test(newVal));

    //                function isSatisfied(criteria) {
    //                    return criteria ? 1 : 0;
    //                }
    //            }, true);
    //        },
    //        template: '<div class="progress" style="height:20px;margin-top:5px" ng-switch on="strength" >' +
    //        '<div ng-switch-when="0" class="progress-bar progress-bar-danger" style="width: 20%">Weak</div>' +
    //      '<div ng-switch-when="1" class="progress-bar progress-bar-danger" style="width: 25%">Weak</div>' +
    //      '<div ng-switch-when="2" class="progress-bar progress-bar-warning" style="width:50%">Medium</div>' +
    //      '<div ng-switch-when="3" class="progress-bar progress-bar-warning" style="width:75%">Medium</div>' +
    //      '<div ng-switch-when="4" class="progress-bar progress-bar-success" style="width: 100%">Strong</div>' +          
    //          '</div>'
    //    }
    //}
    //]);
}());