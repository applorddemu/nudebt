﻿(function () {
    'use strict';

    angular
        .module('app.layout').directive('disableContents', function () {
    return {
        compile: function (tElem, tAttrs) {

            var inputs = tElem.find('input');
            var textarea = tElem.find('textarea');
            var multiselect = tElem.find('select');
            var button = tElem.find('button');
            var images = tElem.find('image');
            //var aTag = tElem.find('a');
            //aTag.attr('ng-disabled', tAttrs['disableContents']);
            inputs.attr('ng-disabled', tAttrs['disableContents']);
            textarea.attr('ng-disabled', tAttrs['disableContents']);
            multiselect.attr('ng-disabled', tAttrs['disableContents']);
            button.attr('ng-disabled', tAttrs['disableContents']);
            images.attr('ng-disabled', tAttrs['disableContents']);
        }
    }
        });
}());