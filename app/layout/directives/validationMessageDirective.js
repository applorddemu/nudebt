﻿(function () {
    'use strict';

    angular
        .module('app.layout')
        .directive('validationMessage', [function () {
            return {
                restrict: 'E',
                require: '^form',
                link: function (scope, element, attrs, formCtrl) {
                    scope.form = formCtrl;
                },
                template: "<span class='error fade in validationmessage' ng-show='form.$submitted && form.$invalid'> <b>Please complete all highlighted fields to continue </b></span>"
            };
        }]);
}());
