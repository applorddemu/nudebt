﻿(function () {
    'use strict';

    angular
        .module('app.layout')
    
        .directive('passwordStrength', [
    function () {
        return {
            require: 'ngModel',
            restrict: 'E',
            scope: {
                password: '=ngModel'
            },

            link: function (scope, elem, attrs, ctrl) {
                scope.$watch('password', function (newVal) {
                    scope.strength = isSatisfied(newVal && newVal.length >= 8) +
                      isSatisfied(newVal && /[A-Z]/.test(newVal)) +
                      isSatisfied(newVal && /(?=.*\W)/.test(newVal)) +
                      isSatisfied(newVal && /\d/.test(newVal));

                    function isSatisfied(criteria) {
                        return criteria ? 1 : 0;
                    }
                }, true);
            },
            template: '<div class="progress" style="height:20px;margin-top:5px" ng-switch on="strength" >' +
            '<div ng-switch-when="0" class="progress-bar progress-bar-danger" style="width: 20%">Weak</div>' +
          '<div ng-switch-when="1" class="progress-bar progress-bar-danger" style="width: 25%">Weak</div>' +
          '<div ng-switch-when="2" class="progress-bar progress-bar-warning" style="width:50%">Medium</div>' +
          '<div ng-switch-when="3" class="progress-bar progress-bar-warning" style="width:75%">Medium</div>' +
          '<div ng-switch-when="4" class="progress-bar progress-bar-success" style="width: 100%">Strong</div>' +
              '</div>'
        }
    }
        ]);

}());