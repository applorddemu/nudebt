﻿(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('TermsAndConditionModalController', controller);

    controller.$inject = [
        '$modalInstance', 'title', 'message', '$sanitize'];
    /* @ngInject */
    function controller($modalInstance, title, message, $sanitize) {

        var vm = this;
        vm.title = title;
        vm.message = message;

        vm.ok = function () {
            $modalInstance.close();
        };

        return vm;
    }
})();