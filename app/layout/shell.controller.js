(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('ShellController', shellController);

    shellController.$inject = ['session', 'authService', 'environment', '$q', 'navigation', 'EnumService', 'modalService', '$rootScope', '$localStorage', '$timeout'];
    /* @ngInject */

    function shellController(session, authService, env, $q, navigation, enumService, modalService, $rootScope, $localStorage, $timeout) {
        var vm = this;
        vm.isAuthenticated = authService.isAuthenticated;
        vm.getUsername = function () { return session.username; };
        if ($localStorage.userDetails != null && $localStorage.userDetails != undefined && $localStorage.userDetails != "")
            $rootScope.layoutHeaderChanges = false;
        else
            $rootScope.layoutHeaderChanges = true;

        vm.layoutHeaderChanges = $rootScope.layoutHeaderChanges;
        vm.roleTypeEnum = enumService.RoleType;
        $localStorage.ProfileInfo = {};

        vm.logout = function () {
            $rootScope.ProfileInfo = {};
            $rootScope.ProfileInfo = "";
            $rootScope.layoutHeaderChanges = true;
            $localStorage.isAuthenticationlogin = false;
            navigation.goToLogin();
            $localStorage.$reset();
            $timeout(function () {
                //location.reload(true);
                //$route.reload();
            }, 100);
        }

        vm.versionNotificationModalShowing = false;

        vm.refreshApp = function () {
            location.reload(true);
        };

        vm.gotoTop = function () {
            $('html, body').animate({ scrollTop: 0 }, 500);
        };

        activate();

        function activate() {
            $('.scroll-to-top').fadeOut(0);
            vm.isOnLoginOrRegisterScreen = navigation.isOnLoginOrRegisterScreen;
        }

        vm.changePassword = function () {
            modalService.changePassword();
        }
    }
})();
