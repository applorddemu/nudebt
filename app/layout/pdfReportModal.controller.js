﻿(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('PdfReportModalController', controller);

    controller.$inject = ['$modalInstance', 'reportName', 'pdfUrl'];
    /* @ngInject */
    function controller($modalInstance, reportName, pdfUrl) {

        var vm = this;
        vm.reportName = reportName;
        vm.pdfUrl = pdfUrl;
        vm.ok = function () {
            $modalInstance.close();
        };

        activate();

        function activate() {
        }

        return vm;
    }
})();
