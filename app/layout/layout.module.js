(function() {
    'use strict';

    angular.module('app.layout', ['app.core']).filter('trustasHtml', function ($sce) { return $sce.trustAsHtml; });
})();
