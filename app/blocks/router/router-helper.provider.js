/* Help configure the state-base ui.router */
(function() {
    'use strict';

    angular
        .module('blocks.router')
        .provider('routerHelper', routerHelperProvider);

    routerHelperProvider.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];
    /* @ngInject */
    function routerHelperProvider($locationProvider, $stateProvider, $urlRouterProvider) {
        /* jshint validthis:true */
        var config = {
            docTitle: undefined,
            resolveAlways: {}
        };
   
        $locationProvider.html5Mode(false);
        //$locationProvider.html5Mode(true).hashPrefix('!');

        this.configure = function(cfg) {
            angular.extend(config, cfg);
        };

        this.$get = RouterHelper;
        RouterHelper.$inject = ['$location', '$rootScope', '$state', 'logger', 'authService', 'navigation', 'InsufficientPermissionService', '$window', 'environment','$localStorage'];
        /* @ngInject */
        function RouterHelper($location, $rootScope, $state, logger, authService, navigation, InsufficientPermissionService, $window, env, $localStorage) {
            var handlingStateChangeError = false;
            var hasOtherwise = false;
            var stateCounts = {
                errors: 0,
                changes: 0
            };
            $rootScope.$state = $state;

            var service = {
                configureStates: configureStates,
                getStates: getStates,
                stateCounts: stateCounts
            };

            init();

            return service;

            ///////////////

            function configureStates(states, otherwiseState) {
                states.forEach(function(state) {
                    state.config.resolve = angular.extend(state.config.resolve || {}, config.resolveAlways);
                    if (env.virtualPathPrefix!='') {
                        state.config.templateUrl = env.virtualPathPrefix + state.config.templateUrl;
                    }
                    $stateProvider.state(state.state, state.config);
                });
                
                if (otherwiseState && !hasOtherwise) {
                    hasOtherwise = true;
                    $urlRouterProvider.otherwise(function ($injector, $location) {
                        var isAuthenticated = authService.isAuthenticated();
                        var state = $injector.get('$state');
                        if (isAuthenticated)
                            state.go(otherwiseState);
                        else
                            state.go('login');
                        return $location.path();
                    });
                    //$urlRouterProvider.otherwise(otherwisePath);
                }
            }

            function handleRoutingStart() {

                $rootScope.$on('$stateChangeStart', function (event, next, nextParams, previous, previousParams) {
                    $state.previousState = previous;
                    if (next.settings && next.settings.mustBeAuthenticated && ($localStorage.isAuthenticationlogin == false || $localStorage.isAuthenticationlogin == undefined)) {
                        //user not authenticated, redirect to login page
                        event.preventDefault();
                        navigation.goToLogin();
                        return;
                    }

                    //if (next.settings && next.settings.requiredPermission && !authService.hasRequiredPermission(next.settings.requiredPermission)) {
                    //    event.preventDefault();
                    //    InsufficientPermissionService.tellUserTheyDontHavePermissions(next.title);
                    //    return;
                    //}

                    //handle redirection to default child states
                    if (next.redirectTo){
                        event.preventDefault();
                        $state.go(next.redirectTo);
                    }
                });
            }

            function handleRoutingErrors() {
                // Route cancellation:
                // On routing error, go to the dashboard.
                // Provide an exit clause if it tries to do it twice.
                $rootScope.$on('$stateChangeError',
                    function(event, toState, toParams, fromState, fromParams, error) {
                        if (handlingStateChangeError) {
                            return;
                        }
                        stateCounts.errors++;
                        handlingStateChangeError = true;
                        var destination = (toState &&
                            (toState.title || toState.name || toState.loadedTemplateUrl)) ||
                            'unknown target';
                        var msg = 'Error routing to ' + destination + '. ' +
                            (error.data || '') + '. <br/>' + (error.statusText || '') +
                            ': ' + (error.status || '');
                        
                       // console.log(msg, [toState]);
                        logger.warning(msg, [toState]);
                        $location.path('/core/404');
                    }
                );
            }

            function init() {
                handleRoutingStart();
                handleRoutingErrors();
                updateDocTitle();
            }

            function getStates() { return $state.get(); }

            function updateDocTitle() {
                $rootScope.$on('$stateChangeSuccess',
                    function (event, toState, toParams, fromState, fromParams) {
                        $window.scrollTo(0, 0);
                        stateCounts.changes++;
                        handlingStateChangeError = false;
                        var title = config.docTitle + ' ' + (toState.title || '');
                        $rootScope.title = title; // data bind to <title>
                    }
                );
            }
        }
    }
})();
