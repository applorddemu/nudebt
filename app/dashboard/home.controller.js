﻿(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashboardHomeController', controller);

    controller.$inject = ['session', 'homeService', 'EnumService', '$q', '$linq', 'navigation', '$interval', 'modalService', 'editProfileService', '$modal', 'environment', '$localStorage', '$rootScope', '$scope','applicationSettings'];
    /* @ngInject */
    function controller(session, homeService, enumService, $q, $linq, navigation, $interval, modalService, editProfileService, $modal, environment, $localStorage, $rootScope, $scope,applicationSettings) {
        angular.isUndefinedOrNull = function (val) {
            return angular.isUndefined(val) || val === null || val === "";
        }
        if ($localStorage.userDetails != null && $localStorage.userDetails != undefined && $localStorage.userDetails != "")
            $rootScope.layoutHeaderChanges = false;
        else
            $rootScope.layoutHeaderChanges = true;
        var vm = this;

        if (!angular.isUndefinedOrNull($localStorage.userDetails) && !angular.isUndefinedOrNull($localStorage.DebtorName))
            vm.DebtorName = $localStorage.DebtorName;



        vm.profile = {};
        $rootScope.percent;
        $scope.options = {
            animate: {
                duration: 0,
                enabled: false
            },
            barColor: '#FFCE58',
            trackColor: '#f5f5f5',
            scaleColor: false,
            lineWidth: 15,
            lineCap: 'square',
            size: '200'
        };


        $scope.percent1 = 25;
        $scope.options1 = {
            animate: {
                duration: 0,
                enabled: false
            },
            barColor: '#3851A1',
            trackColor: '#f5f5f5',
            scaleColor: false,
            lineWidth: 15,
            lineCap: 'square',
            size: '200'
        };


        function loadProfile() {
            editProfileService.getProfileDetails($localStorage.userDetails.C_MatterID).then(function (response) {
                if (response != null)
                    vm.profile = response;

                if (vm.profile.TitleName == "")
                    vm.profile.TitleName = "-";

                if (vm.profile.Initials == null || vm.profile.Initials == "-")
                    vm.profile.Initials = "-";

                if (vm.profile.SA_ID == null)
                    vm.profile.SA_ID = "-";

                if (vm.profile.Gender == null)
                    vm.profile.Gender = "-";

                if (vm.profile.CellNumber == null)
                    vm.profile.CellNumber = "-";

                if (vm.profile.Email == null)
                    vm.profile.Email = "-";

                vm.profile.ProfileStatusTypeName = $localStorage.ProfileStatusTypeName;
                if (vm.profile.TotalRewardPoints == null)
                    vm.profile.TotalRewardPoints = 0;

                editProfileService.getStatus($localStorage.userDetails.C_MatterID).then(function (response) {
                    if (response != null) {
                        vm.profile.NextStatus = response.ProfileNextStatus;
                        if (vm.profile.NextStatus == applicationSettings.ProfileStatus)
                            vm.isProfileStatusUpdate = true;
                        else
                            vm.isProfileStatusUpdate = false;
                    }
                });

                editProfileService.GetRewardStatusImageBar(response).then(function (result) {
                    if (result != null) 
                        vm.profile.ProfileStatusImage = result;

                }, function (err) {
                    if (!angular.isUndefined(err.data))
                        vm.validationErrors = err.data.errorList;               
                });
            },
            function (err) {
                if (!angular.isUndefined(err.data))
                    vm.validationErrors = err.data.errorList;                               
            });
        }

        vm.profileClick = function () {
            navigation.goToProfile();
            $rootScope.profileDetails = $localStorage.userDetails;
        }

        vm.getCustomerApplications = function () {
            homeService.getAppDetails()
                .then(function (results) {
                    if (results != null) {
                        vm.paymentList = results;
                        angular.forEach(vm.paymentList, function (item, value) {
                            if (item != null) {
                                item.M_OutstandingAmt = numberWithCommas(item.M_OutstandingAmt);
                                item.M_CapitalAmt = numberWithCommas(item.M_CapitalAmt);
                            }
                        });
                        $localStorage.Count = results.length;
                        vm.accCount = $localStorage.Count;                     
                    }
                });
        }

        

        function numberWithCommas(unForNum) {
            //console.log(unForNum);
            var unDeciVal = parseFloat(unForNum).toFixed(2);
            var parts = unDeciVal.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            //console.log(parts[0]);
            //var forNum = Math.abs(Number(parts.join(".")));            
            return parts.join(".");
        }
        
        function activate() {
            vm.getCustomerApplications();
            loadProfile();



            //$q.all([
            //        editProfileService.getProfileDetails($localStorage.userDetails.C_MatterID),
            //        editProfileService.getStatus($localStorage.userDetails.C_MatterID),
            //        editProfileService.GetRewardStatusImageBar(response)
            //]).then(function (responses) {
            //    vm.branch = responses[0];
            //    vm.title = responses[1];
            //    vm.profile = responses[2];
            //    vm.preferableContactMethod = responses[4];
            //});
        }

        activate();

        vm.tab = function (index) {
            vm.tabIndex = index;
        };

        vm.paymentnavigate = function (e) {
            navigation.goToPayment(e.ID);
        }

        vm.breakdownnavigate = function (e) {
            navigation.goToBreakDown(e.ID);
        }
        return vm;
    }
})();