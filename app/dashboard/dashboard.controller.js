﻿(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('MainMenuController', controller);

    controller.$inject = ['navigation', 'session', '$timeout', '$rootScope', '$localStorage', '$scope', 'editProfileService', 'EnumService', 'homeService'];
    function controller(navigation, session, $timeout, $rootScope, $localStorage, $scope, editProfileService, enumService, homeService) {

        var vm = {};
        vm.profile = {};

        angular.isUndefinedOrNull = function (val) {
            return angular.isUndefined(val) || val === null || val === "";
        }

        $rootScope.header = "My Dashboard";
        $rootScope.emailVerified = "";
        $rootScope.profileInfoEmail = "";
        $rootScope.profileDetails = $localStorage.userDetails;
        //$rootScope.ProfileStatus = RewardPtsUpdate();
        vm.isAdmin = $localStorage.isAdmin;
        $scope.outstanding;
        getCustomerApplications();
        RewardPtsUpdate();

        var imagePath = "images/edit_profile/u157_" + $localStorage.ProfileStatusTypeName + ".png";
        vm.profile.ProfileImage = imagePath;
        vm.profile.Name =!angular.isUndefinedOrNull($localStorage.userDetails)? $localStorage.userDetails.D_FullNames:null;
        $rootScope.StatusImage = imagePath;
        $rootScope.profileDetails = $localStorage.userDetails;
         
         

        vm.accountClick = function () {
            $rootScope.header = "My Dashboard";
            navigation.goToDashboard();
            //$rootScope.ProfileInfo = {};
            $rootScope.profileDetails = $localStorage.userDetails;
            vm.count = $localStorage.Count;
        }

        vm.profileClick = function () {
            navigation.goToProfile();
            $rootScope.profileDetails = $localStorage.userDetails;
            //vm.profile.ProfileStatus = RewardPtsUpdate();
        }



        function RewardPtsUpdate() {
            if (!angular.isUndefinedOrNull($localStorage.userDetails) && !angular.isUndefinedOrNull($localStorage.userDetails.C_MatterID)) {
                editProfileService.getTotalRewardPoints($localStorage.userDetails.C_MatterID).then(function (response) {
                    if(!angular.isUndefinedOrNull(response)){
                        vm.TotalRewardPoints = response.TotalRewardPoints;
                       
                        $localStorage.ProfileStatusTypeName = response.ProfileStatusTypeName;
                        var imagePath = "images/edit_profile/u157_" + response.ProfileStatusTypeName + ".png";
                        vm.ProfileImage = imagePath;
                    }
                    else {
                        editProfileService.getTotalRewardPoint($localStorage.userDetails.C_MatterID).then(function (response) {
                            if(!angular.isUndefinedOrNull(response)){
                                vm.profile.TotalRewardPoints = response;
                                var TotalPoints = vm.profile.TotalRewardPoints;

                                if (TotalPoints >= 0 && TotalPoints < 61)
                                    vm.profile.ProfileStatus = "RED";
                                else if (TotalPoints > 60 && TotalPoints <= 110)
                                    vm.profile.ProfileStatus = "BLUE";
                                else if (TotalPoints > 110 && TotalPoints <= 1000)
                                    vm.profile.ProfileStatus = "SILVER";
                                else if (TotalPoints > 1001)
                                    vm.profile.ProfileStatus = "GOLD";

                                $localStorage.userDetails.ProfileStatus = vm.profile.ProfileStatus;
                                $rootScope.ProfileStatus = vm.profile.ProfileStatus;
                                var imagePath = "images/edit_profile/u157_" + $rootScope.ProfileStatus + ".png";
                                vm.StatusImage = imagePath;
                                $localStorage.ProfileStatus = $rootScope.ProfileStatus;
                                $localStorage.profileImage = vm.StatusImage;
                            }
                        });
                    }
                });
            }
        }

        function getCustomerApplications() {
            if (!angular.isUndefinedOrNull($localStorage.userDetails)) {
                homeService.getMatterCountByClientId()
                    .then(function (resCount) {
                        if (!angular.isUndefinedOrNull(resCount)) {
                            vm.paymentList = resCount;
                            //angular.forEach(vm.paymentList, function (item, value) {
                            //    if (item.M_ID != "") {
                            //        //item.paidAmt = (item.M_CapitalAmt - item.M_OutstandingAmt).toFixed(2);
                            //        //item.percent = (item.paidAmt / item.M_CapitalAmt) * 100;
                            //        //  results[item].percent = item.percent;
                            //        item.paidAmt = (item.PaidAmount).toFixed(2);
                            //        item.percent = (item.PaidPercentage).toFixed(1);
                            //    }
                            //});
                            $localStorage.Count = resCount;
                            vm.count = $localStorage.Count;
                            //$rootScope.percent = Math.abs(results.percent);
                        }
                    });
            }                    
        }

        return vm;
    }
})();