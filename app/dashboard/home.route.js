﻿(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.home',
                config: {
                    templateUrl: '/app/dashboard/home.html',
                    controller: 'DashboardHomeController',
                    controllerAs: 'vm',
                    title: 'Home',
                    ncyBreadcrumb: { label: 'Home' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();
