﻿(function () {
    'use strict';
    angular
        .module('app.payment')
        .controller('PaymentController', controller);

    controller.$inject = ['navigation', 'session', '$timeout', 'modalService', 'EnumService', 'paymentService', '$q', '$window', 'breakdownService', 'messagingService', 'editProfileService',
        'id', '$rootScope', '$linq', '$localStorage', 'utilityService', '$interval', 'log', '$scope'];
    /* @ngInject */
    function controller(navigation, session, $timeout, modalService, EnumService, paymentService, $q, $window, breakdownService, messagingService, editProfileService,
        id, $rootScope, $linq, $localStorage, utilityService, $interval, log) {
        var vm = {};
        vm.validationErrors = "";
        vm.Account = "";
        vm.OutstandingAmount = "";
        vm.mId = id;
        vm.payment = {};
        $rootScope.header = "Payment Plan";
        vm.options = {
            format: "DD/MM/YYYY",

        };
        angular.isUndefinedOrNull = function (val) {
            return angular.isUndefined(val) || val === null;
        }
        //var forbidden=['2017-07-25','2017-07-24']

        //$('#datepicker').datepicker({
        //    beforeShowDay:function(Date){
        //        var curr_date = Date.toJSON().substring(0,10);

        //        if (forbidden.indexOf(curr_date)>-1) return false;        
        //    }
        //})

        //timeout page starts
        vm.idleTime = 0;
        var idleInterval = setInterval(timerIncrement, 300000);
        function timerIncrement() {
            vm.idleTime++;
            if (vm.idleTime >= 1) {
                Preload();
            }
        }
        $('#paymentId').on('change keypress', function (e) {
            clearInterval(idleInterval);
            idleInterval = setInterval(timerIncrement, 300000);
            vm.idleTime = 0;
        });
        function Preload() {
            vm.idleTime = 0;
            clearInterval(idleInterval);
            navigation.goToDashboard();
            $rootScope.header = "View Accounts";
        }
        //timeout page Ends

        //vm.timeInterval = $interval(function () {
        //    navigation.goToDashboard();
        //    $rootScope.header = "Dashboard";
        //    $interval.cancel(vm.timeInterval);
        //}, 300000);
        paymentService.getDisabledDateList().then(function (response) {
            if (response != null) {
                vm.disabledDates = [];
                vm.disabledDates = response;
            }
        });

        var date = new Date();
        vm.minDate = new Date(date.setDate(date.getDate() + 2));

        vm.update = function () {
            vm.options.minDate = vm.minDate;
            //console.log(vm.payment.Date);
            var disabledDates = [];
            for (var y = 2010; y < 2060; y++) {
                disabledDates.push(y + "/01/01", y + "/01/02", y + "/03/21", y + "/04/14", y + "/04/17", y + "/04/27", y + "/05/01", y + "/06/16", y + "/08/09", y + "/09/24", y + "/09/25", y + "/12/16", y + "/12/25", y + "/12/26");
            }
            // var y = dateSel.getFullYear();
            vm.options.disabledDates = disabledDates; //[y + "/01/01", y + "/01/02", y + "/03/21", y + "/04/14", y + "/04/17", y + "/04/27", y + "/05/01", y + "/06/16", y + "/08/09", y + "/09/24", y + "/09/25", y + "/12/16", y + "/12/25", y + "/12/26", ];
            //console.log(vm.options.disabledDates);

            //if (vm.payment.Date) {
            //    var dateSel = new Date(vm.payment.Date);
            //    if (!isNaN(dateSel)) {
            //        var y = dateSel.getFullYear();
            //        vm.options.disabledDates = [y + "/01/01", y + "/01/02", y + "/03/21", y + "/04/14", y + "/04/17", y + "/04/27", y + "/05/01", y + "/06/16", y + "/08/09", y + "/09/24", y + "/09/25", y + "/12/16", y + "/12/25", y + "/12/26", ];
            //        console.log(vm.options.disabledDates);
            //    }
            //} else {
            //    //var defDate = moment(new Date()).format("DD/MM/YYYY")
            //    var defDate = moment(new Date());
            //    if (defDate) {
            //        var dateSel = new Date(defDate);

            //    }
            //}
        };

        vm.update();



        vm.changeDate = function (date){
            // `e` here contains the extra attributes
            //console.log(date);
            //alert(date);
        };

        vm.navigateDashBoard = function () {
            clearInterval(idleInterval);
            if ($rootScope.isBreakDown) {
                navigation.goToBreakDown(id);
                $rootScope.header = "Balance Breakdown";
            }
            else {
                navigation.goToDashboard();
                $rootScope.header = "My Dashboard";
            }
            $rootScope.isBreakDown = false;
        }
        vm.tab = function (index) {
            vm.tabIndex = index;
            if (vm.tabIndex == 2) {
                if (angular.isUndefined(vm.payment.IPAddres) || !vm.payment.IPAddres)
                    vm.getUserIp();
            }

        };

        vm.termsAndCondition = function (url) {
            modalService.openPdfReportModal('Terms & Condition', url);
            // $window.open(url);
        }
        vm.getZapper = function (url) {
            $window.open(url);
        }
        vm.getDate = function (getdate) {
            var date = getdate.split('/');
            var dateFormat = date[1] + '/' + date[0] + "/" + date[2];
            return dateFormat;
        }

        vm.save = function (payment, form) {
            messagingService.broadcastCheckFormValidatity();
            if (!form.$invalid) {
                vm.validationErrors = null;
                //  ************** Debit Order Service Code Start  ***************
                var postdata = {};

                if (angular.isUndefinedOrNull(payment.Frequency) && payment.Frequency == 0)
                    payment.Frequency = 1;
                if (angular.isUndefinedOrNull(payment.NumberOfCollection) && payment.NumberOfCollection == 0)
                    payment.NumberOfCollection = 1;

                postdata = payment;
                if (payment.Frequency == EnumService.Frequency.Monthly && payment.CollectionAmount > 50000)
                    return log.error("Error", "Month's transaction exceeds 15000");

                else if (payment.Frequency == EnumService.Frequency.FortNightly && payment.CollectionAmount > 15000)
                    return log.error("Error", "Single transaction exceeds 15000");

                if (angular.isUndefinedOrNull(payment.IPAddres))
                    return log.error("Error", "IPAddress not Detected");

                switch (payment.Frequency) {
                    case EnumService.Frequency.OnceOff:
                        payment.FrequencyValue = "O";
                        break;
                    case EnumService.Frequency.Monthly:
                        payment.FrequencyValue = "M";
                        break;
                    case EnumService.Frequency.Weekly:
                        payment.FrequencyValue = "W";
                        break;
                    case EnumService.Frequency.FortNightly:
                        payment.FrequencyValue = "F";
                        break;
                    default:
                        break;

                }
                //if (vm.payment.Branch)
                //    postdata.BranchCode = vm.payment.Branch;
                postdata.ActionDate = payment.Date;// paymentService.changeGlobalDateFormat(payment.Date);
                postdata.ClientIdentification = vm.matterNumber;
                postdata.ClientReference = vm.matterNumber;
                postdata.InternalReference = vm.matterNumber;
                postdata.BankReference = vm.matterNumber;
                postdata.Matter_Id = vm.matter.M_ID;
                postdata.Client_Id = vm.matter.M_ClientID;
                postdata.OutstandingAmt = vm.OutstandingAmount;
                postdata.DebtorName = $localStorage.DebtorName;
                //postdata.NumberOfCollection = payment.collection;
                //$("#myModalCell").modal("show");
                var data = JSON.stringify(postdata);
                $("#confirm-submit").modal("hide");
                navigation.goToPaymentRedirect(data);
                $rootScope.header = "Payment Process";
                //paymentService.save(postdata).then(function (response) {
                //    if (response != null && response.Result) {
                //        editProfileService.getTotalRewardPoints($localStorage.userDetails.C_MatterID).then(function (response) {
                //            if (!angular.isUndefinedOrNull(res)) {
                //                vm.TotalRewardPoints = res.TotalRewardPoints;
                //            }
                //        });
                //        $("#confirm-submit").modal("hide");
                //        modalService.paymentMessage(TotalRewardPoints);
                //    }
                //    else if (response == null || response.Result == false) {
                //        $("#confirm-submit").modal("hide");
                //        $("#myModalCell").modal("hide");
                //        return log.error("Error", "Requested Payment Failed");
                //    }
                //},
                //function (err) {
                //    if (!angular.isUndefined(err.data))
                //        vm.validationErrors = err.data.errorList;

                //        $("#myModalCell").modal("hide");
                //        $("#confirm-submit").modal("hide");                              
                //    });

                //  ************** Debit Order Service Code End  ***************


            }
        }

        vm.submit = function (payment, form) {
            messagingService.broadcastCheckFormValidatity();
            if (!form.$invalid) {
                $("#confirm-submit").modal("show");
            }
        }
        vm.emailAddress = false;
        vm.zappersave = function (zapper, form) {
            messagingService.broadcastCheckFormValidatity();
            if (!form.$invalid) {
                zapper.Matter_Id = vm.matter.M_ID;
                zapper.Client_Id = vm.matter.M_ClientID;
                zapper.OutstandingAmt = vm.OutstandingAmount;
                zapper.CollectionAmount = vm.OutstandingAmount;
                zapper.AccountType = 3;
                zapper.IPAddres = vm.payment.IPAddres;
                zapper.ActionDate = new Date();
                zapper.ReferenceNo = $localStorage.userDetails.C_DebtorIdentityNo;
                paymentService.getZapperCode(zapper).then(function (response) {
                    vm.qrCode = response;
                    vm.emailAddress = true;
                });
            }
        }



        function activate() {
            $q.all([

                  paymentService.getBranchType(vm.refresh),
                  paymentService.getFrequencyType(vm.refresh),
                  paymentService.getCollectionType(vm.refresh),
                  breakdownService.getCapitalAmtDetails(vm.mId),
                  paymentService.getIdentificationType(vm.refresh),
                  paymentService.getAccountType(vm.refresh),

                  editProfileService.getProfileDetails($localStorage.userDetails.C_MatterID),
                    paymentService.getDisabledDateList(vm.refresh)
            ])
              .then(function (responses) {
                  vm.branch = responses[0];
                  vm.FrequencyList = responses[1];
                  vm.CollectionList = responses[2];
                  vm.matter = responses[3];
                  vm.Account = responses[3].M_CreditorName;
                  vm.matterNumber = responses[3].M_ID;
                  vm.OutstandingAmount = responses[3].M_OutstandingAmt != null ? responses[3].M_OutstandingAmt.toFixed(2) : 0;
                  vm.identificationList = responses[4];
                  vm.payment.NumberOfCollection = vm.CollectionList[0].Value;
                  vm.payment.Frequency = vm.FrequencyList[0].Key;
                  if (vm.OutstandingAmount != 0)
                      vm.payment.CollectionAmount = parseFloat(vm.OutstandingAmount / vm.CollectionList[0].Key).toFixed(2);
                  else
                      vm.payment.CollectionAmount = 0;

                  vm.accountTypeList = responses[5].slice(0, -1);
                  vm.payment.Email = responses[6].Email;
                  vm.payment.MobileNumber = responses[6].CellNumber;
                  vm.payment.AccountNumber = responses[6].AccountNumber;

                  vm.payment.BranchCode = $linq.Enumerable().From(vm.branch).Where(function (x) {
                      return x.Key == responses[6].BranchCode;
                  }).Select(function (x) {
                      return x.Key;
                  }).FirstOrDefault();
                  vm.GetPreviousPaymentChartList(responses[3].M_ID, responses[3].M_IDX);

              });
        }

        vm.GetPreviousPaymentChartList = function (matterId, matterIndexId) {
            breakdownService.getPreviousPaymentChartDetail(matterId, matterIndexId)
            .then(function (results) {
                if (results.length != 0) {
                    vm.OutstandingAmount = numberWithCommas(results[0].balanceamt);
                }
            });
        }

        //var formatter = new Intl.NumberFormat('af-ZA', {
        //    style: 'currency',
        //    currency: 'ZAR',
        //    minimumFractionDigits: 2,
        //});

        function numberWithCommas(unForNum) {
            var unDeciVal = parseFloat(unForNum).toFixed(2);
            var parts = unDeciVal.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            //console.log(parts[0]);
            //var forNum = Math.abs(Number(parts.join(".")));            
            return parts.join(".");
        }

        vm.termsAndConditionChange = function () {
            vm.getUserIp();
        }


        $("#datetimepicker5").datetimepicker({
            disabledDates: [
               "07/26/2017"
            ]
        });


        vm.getUserIp = function () {
            //utilityService.getUserIpAddress()
            //    .then(function (results) {
            //        vm.payment.IPAddres = results.ip;
            //        $rootScope.$apply();
            //    });
            utilityService.getIpAddress()
                .then(function (results) {
                    vm.payment.IPAddres = results;
                    $rootScope.$apply();
                });
        };

        // Dropdown chnage                                     
        vm.changedValue = function (item) {
            vm.payment.CollectionAmount = $linq.Enumerable().From(vm.CollectionList).Where(function (x) {
                return x.Value == item
            }).Select(function (x) {
                var xNum = vm.OutstandingAmount.replace(/[^\d\.]/g, "");
                return numberWithCommas(parseFloat(xNum / x.Key).toFixed(2));
            }).FirstOrDefault();
        }


        activate();
        return vm;
    }
})();