﻿(function () {
    'use strict';

    angular
        .module('app.payment')
        .controller('paymentMessageController', controller);

    controller.$inject = ['$stateParams', 'navigation','TotalRewardPoints','$modalInstance','$rootScope'];
    /* @ngInject */
    function controller($stateParams, navigation, TotalRewardPoints, $modalInstance, $rootScope) {

        var vm = {};
        vm.TotalRewardPoints = TotalRewardPoints;
        vm.close = function () {
            $modalInstance.close();
            navigation.goToDashboard();
            $rootScope.header = "My Dashboard";
        }
        return vm;

    }
})();