﻿(function () {
    'use strict';

    angular
        .module('app.payment')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {

        return [
      {
          state: 'dashboard.paymentMessage',
          config: {
             // url: 'paymentcomplete',
              templateUrl: '/app/payment/paymentmessage.html',
              controller: 'paymentMessageController',
              controllerAs: 'vm',
              title: 'Payment Plan',
              ncyBreadcrumb: { label: 'payment Plan' },
              settings: {
                  mustBeAuthenticated: false
              },           

          }
      }
        ];

    }
})();
