﻿(function () {
	'use strict';
	angular
        .module('app.payment')
        .controller('PaymentredirectController', controller);
	debugger;
	controller.$inject = ['navigation', 'modalService', 'paymentService', 'editProfileService', 'log', '$rootScope', '$controller', '$stateParams','$localStorage'];
	/* @ngInject */
	function controller(navigation, modalService, paymentService, editProfileService, log, $rootScope, $controller, $stateParams, $localStorage) {

		var vm = {};
		//$controller('PaymentController', { vm: vm });
		vm.paymentprocess = true;
		vm.backbutton = false;
		

		$("#paymentfailed").modal("hide");
		$rootScope.header = "Payment Process";
		if ($stateParams.data != null) {
			var postdata = JSON.parse($stateParams.data);
			paymentService.save(postdata).then(function (response) {
				if (response != null && response.Result) {
					editProfileService.getTotalRewardPoints($localStorage.userDetails.C_MatterID).then(function (res) {
						if (!angular.isUndefinedOrNull(res)) {
							vm.TotalRewardPoints = res.TotalRewardPoints;
						}
						vm.paymentprocess = false;
						vm.backbutton = true;
						modalService.paymentMessage(vm.TotalRewardPoints);
					});
				}
				else if (response == null || response.Result == false || response == "") {
					setTimeout(function () {
						vm.paymentprocess = false;
						vm.backbutton = true;
						$("#paymentfailed").modal("show");
					}, 5000);
				}
			},
				function (err) {
					if (!angular.isUndefined(err.data))
						vm.validationErrors = err.data.errorList;
				});
		}
		else {
			navigation.goToDashboard();
			$rootScope.header = "My Dashboard";
		}
		vm.navigateDashBoard = function () {
			$("#paymentfailed").modal("hide");
			navigation.goToDashboard();
			$rootScope.header = "My Dashboard";
		}
		return vm;
	}
})();