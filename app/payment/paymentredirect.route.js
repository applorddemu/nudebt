﻿(function () {
	'use strict';

	angular
        .module('app.payment')
        .run(appRun);

	appRun.$inject = ['routerHelper'];
	/* @ngInject */
	function appRun(routerHelper) {
		routerHelper.configureStates(getStates());
	}

	function getStates() {

		return [
		  {
		  	state: 'dashboard.paymentProcess',
		  	config: {
		  		url: 'paymentprocess',
		  		templateUrl: '/app/payment/paymentredirect.html',
		  		controller: 'PaymentredirectController',
		  		controllerAs: 'vm',
		  		title: 'Payment Process',
		  		ncyBreadcrumb: { label: 'payment Process' },
		  		settings: {
		  			mustBeAuthenticated: true
		  		},
		  		//resolve: {
		  		//	data: ['$stateParams', function ($stateParams) {
		  		//		return $stateParams.data;
		  		//	}]
		  	    //},
		  		params: { data: null }
		  	}
		  }
		];
	}
})();