﻿(function () {
    'use strict';

    angular.module('app.payment', ['app.core'])
        .run(['activeUserSession', function (activeUserSession) {
            activeUserSession.load();
        }]);;
})();