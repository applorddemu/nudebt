﻿(function () {
    'use strict';

    angular
        .module('app.payment')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {

        return [
      {
          state: 'dashboard.payment',
          config: {
              url: 'payment/{id}',
              templateUrl: '/app/payment/payment.html',
              controller: 'PaymentController',
              controllerAs: 'vm',
              title: 'Payment',
              ncyBreadcrumb: { label: 'payment' },
              settings: {
                  mustBeAuthenticated: true
              },
              resolve: {
                  id: ['$stateParams', function ($stateParams) {
                      return $stateParams.id;
                  }]
              },

          }
      }
        ];
     
    }
})();
