﻿(function () {
    'use strict';

    angular
        .module('app.paymentlog')
        .controller('PaymentlogController', controller);

    controller.$inject = ['session', 'paymentlogService', 'EnumService', '$q', '$linq', 'navigation', '$interval', 'modalService', '$modal', 'environment', '$localStorage', '$rootScope'];
    /* @ngInject */
    function controller(session, paymentlogService, enumService, $q, $linq, navigation, $interval, modalService, $modal, environment, $localStorage, $rootScope) {
        $rootScope.layoutHeaderChanges = null;
        var vm = this;
        $rootScope.header = "Payment Log";
        function paymentLogList() {
            paymentlogService.getPaymentLog()
                     .then(function (results) {
                         vm.paymentlogList = results;
                     });
        }
        angular.isUndefinedOrNullOrEmpty = function (val) {
            return angular.isUndefined(val) || val === null || val === "";
        }
        vm.search = function () {
            var postData = {};
            if (!angular.isUndefinedOrNullOrEmpty(vm.searchText)) {
                postData.searchQuery = vm.searchText;
                paymentlogService.getPaymentLogList(postData).then(function (response) {
                    vm.paymentlogList = response;
                });
            }
        }
        vm.refresh = function () {
            paymentLogList();
            vm.searchText = "";
        }
        activate();
        function activate() {
            paymentLogList();
        }
        return vm;
    }
})();