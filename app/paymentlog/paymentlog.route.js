﻿(function () {
    'use strict';

    angular
        .module('app.paymentlog')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
               
                state: 'dashboard.paymentlog',
            config: {
                    url: 'paymentlog',
                    templateUrl: '/app/paymentlog/paymentlog.html',
                    controller: 'PaymentlogController',
                    controllerAs: 'vm',
                    title: 'Paymentlog',
                    ncyBreadcrumb: { label: 'Paymentlog' },
                    settings: {
                        mustBeAuthenticated: false
                    }
                }
            }
        ];
    }
})();