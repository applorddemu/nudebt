﻿(function () {
    'use strict';

    angular
        .module('app.editprofile')
        .controller('EditProfileController', controller);

    controller.$inject = ['session', 'editProfileService', 'EnumService', '$q', '$scope', '$linq', 'navigation', '$interval', 'modalService', '$modal', 'environment', '$localStorage', '$rootScope', 'paymentService', 'messagingService', 'log', '$controller', 'applicationSettings'];
    /* @ngInject */
    function controller(session, editProfileService, enumService, $q, $scope, $linq, navigation, $interval, modalService, $modal, environment, $localStorage, $rootScope, paymentService, messagingService, log, $controller, applicationSettings) {
        $rootScope.layoutHeaderChanges = null;
        $rootScope.header = "Profile";
        $rootScope.ProfileStatus = "";
        $rootScope.NextStatus = "";
        var vm = this;
        var clientUrl = environment.clientBaseUrl;
        var TotalPoints;
        vm.options = {
            format: "DD/MM/YYYY",
        };

        angular.isUndefinedOrNull = function (val) {
            return angular.isUndefined(val) || val === null || val === "";
        }

        if (!angular.isUndefinedOrNull($localStorage.userDetails))
            $rootScope.layoutHeaderChanges = false;
        else
            $rootScope.layoutHeaderChanges = true;


        vm.profile = {};
        vm.profileExist = {};
        vm.additionalInfo = {};
        vm.response = {};
        vm.rewardPtDetail = {};
        vm.rewardPtDetail.rewardPtDescription = 0;
        vm.rewardPtDetail.RewardPoint = 0;
        vm.otpNotVerified = false;
        vm.otpVerified = false;
        vm.saIdNotVerified = false;
        vm.saIdVerified = false;
        vm.emailNotVerified = false;
        vm.emailVerified = false;
        vm.isVerifyBankAccount = false;
        vm.isVerifiedBankAccount = false;
        vm.profile.TotalRewardPoints = 0;
        vm.isBankVerifyClick = false;
        vm.response.ID = 0;
        var prevNowPlaying = null;



        $scope.openDialog = function () {
            $modal.open({
                templateUrl: 'dialog.html',
                resolve: {
                    email: function () {
                        return $scope.email;
                    }
                },
                controller: function ($scope, $modalInstance, email) {
                    $scope.email = email;
                    $scope.ok = function () {
                        $modalInstance.close({
                            email: this.email,
                            oldEmail: $scope.email
                        });
                    };
                }
            }).result.then(function (p) {
                console.dir(p);
                $scope.email = p.email;
            });
        };

        function signalrListener() {
            try {
                //$.connection.hub.qs = { "userId": $localStorage.userDetails.C_MatterID };
                $.connection.hub.url = config.chatUrl;
                var chat = $.connection.publicHub;

                chat.client.userMessage = function (matterId, isEmailVerified) {
                    //alert("success");
                    if (!angular.isUndefinedOrNull(matterId)
                        && !angular.isUndefinedOrNull($localStorage.userDetails.C_MatterID)
                        && $localStorage.userDetails.C_MatterID == matterId && isEmailVerified == true) {
                        vm.profile.IsEmailVerified = true;
                        vm.profileExist.IsEmailVerified = true;
                        vm.profileExist.Email = vm.profile.Email;
                        vm.profile.Name = angular.isUndefinedOrNull(vm.profile.Name) ? $localStorage.userDetails.D_FullNames : vm.profile.Name;
                        vm.profile.Surname = angular.isUndefinedOrNull(vm.profile.Surname) ? $localStorage.userDetails.D_Surname : vm.profile.Surname;
                        vm.profileExist.Name = angular.isUndefinedOrNull(vm.profileExist.Name) ? $localStorage.userDetails.D_FullNames : vm.profile.Name;
                        vm.profileExist.Surname = angular.isUndefinedOrNull(vm.profileExist.Surname) ? $localStorage.userDetails.D_Surname : vm.profile.Surname;
                        vm.emailNotVerified = false;
                        vm.emailVerified = true;
                        $scope.$apply();
                        //alert(isEmailVerified);
                    }
                };

                $.connection.hub.start().done(function () {
                    console.log('Now connected, connection ID=' + $.connection.hub.id);
                    chat.server.sendMatterId($localStorage.userDetails.C_MatterID, $.connection.hub.id);
                });
            } catch (e) {

            }
        };

        function emailValidCheck() {
            //Email
            if (!angular.isUndefinedOrNull(vm.profile.Email)
                || (!angular.isUndefinedOrNull(vm.profile.IsEmailVerified) && vm.profile.IsEmailVerified == false)
                || (!angular.isUndefinedOrNull(vm.profileExist.IsEmailVerified) && vm.profileExist.IsEmailVerified == true)) {
                vm.profile.Email = angular.isUndefinedOrNull(vm.profileExist.Email) ? null : vm.profileExist.Email;
                vm.profile.Name = angular.isUndefinedOrNull(vm.profile.Name) ? $localStorage.userDetails.D_FullNames : vm.profile.Name;
                vm.profile.Surname = angular.isUndefinedOrNull(vm.profile.Surname) ? $localStorage.userDetails.D_Surname : vm.profile.Surname;
                vm.profileExist.Name = angular.isUndefinedOrNull(vm.profileExist.Name) ? $localStorage.userDetails.D_FullNames : vm.profile.Name;
                vm.profileExist.Surname = angular.isUndefinedOrNull(vm.profileExist.Surname) ? $localStorage.userDetails.D_Surname : vm.profile.Surname;
            }
            else
                vm.profile.Email = null;
        }

        function cellNumberValidCheck() {
            //cellNumber
            if (!angular.isUndefinedOrNull(vm.profile.CellNumber)
                || (!angular.isUndefinedOrNull(vm.profile.IsCellNumberVerified) && vm.profile.IsCellNumberVerified == false)
                || (!angular.isUndefinedOrNull(vm.profileExist.IsCellNumberVerified) && vm.profileExist.IsCellNumberVerified == true))
                vm.profile.CellNumber = angular.isUndefinedOrNull(vm.profileExist.CellNumber) ? null : vm.profileExist.CellNumber;
            else
                vm.profile.CellNumber = null;
        }

        function bankAccountValidCheck() {
            //Bank
            if (!angular.isUndefinedOrNull(vm.profile.Bank)
                || (!angular.isUndefinedOrNull(vm.profile.IsAccountVerified) && vm.profile.IsAccountVerified == false)
                || (!angular.isUndefinedOrNull(vm.profileExist.IsAccountVerified) && vm.profileExist.IsAccountVerified == true))
                vm.profile.Bank = angular.isUndefinedOrNull(vm.profileExist.Bank) ? null : vm.profileExist.Bank;
            else
                vm.profile.Bank = null;

            //Branchcode
            if (!angular.isUndefinedOrNull(vm.profile.BranchCode)
                || (!angular.isUndefinedOrNull(vm.profile.IsAccountVerified) && vm.profile.IsAccountVerified == false)
                || (!angular.isUndefinedOrNull(vm.profileExist.IsAccountVerified) && vm.profileExist.IsAccountVerified == true))
                vm.profile.BranchCode = angular.isUndefinedOrNull(vm.profileExist.BranchCode) ? null : vm.profileExist.BranchCode;

            else
                vm.profile.BranchCode = null;

            //AccountNumber
            if (!angular.isUndefinedOrNull(vm.profile.AccountNumber)
                || (!angular.isUndefinedOrNull(vm.profile.IsAccountVerified) && vm.profile.IsAccountVerified == false)
                || (!angular.isUndefinedOrNull(vm.profileExist.IsAccountVerified) && vm.profileExist.IsAccountVerified == true))
                vm.profile.AccountNumber = angular.isUndefinedOrNull(vm.profileExist.AccountNumber) ? null : vm.profileExist.AccountNumber;
            else
                vm.profile.AccountNumber = null;

            //BranchCodeOther
            if (!angular.isUndefinedOrNull(vm.profile.BranchCodeOther && vm.profile.BranchCode == 7)
                || (!angular.isUndefinedOrNull(vm.profile.IsAccountVerified) && vm.profile.IsAccountVerified == false)
                || (!angular.isUndefinedOrNull(vm.profileExist.IsAccountVerified) && vm.profileExist.IsAccountVerified == true))
                vm.profile.BranchCodeOther = angular.isUndefinedOrNull(vm.profileExist.BranchCodeOther) ? null : vm.profileExist.BranchCodeOther;
            else
                vm.profile.BranchCodeOther = null;
        }

        function VerifiedIconEnableDissable() {
            if (!angular.isUndefinedOrNull(vm.profileExist) && !angular.isUndefinedOrNull(vm.profileExist.IsCellNumberVerified) && vm.profileExist.IsCellNumberVerified == true) {
                vm.otpNotVerified = false;
                vm.otpVerified = true;
            } else {
                vm.otpNotVerified = true;
                vm.otpVerified = false;
            }

            if (!angular.isUndefinedOrNull(vm.profileExist) && !angular.isUndefinedOrNull(vm.profileExist.IsAccountVerified) && vm.profileExist.IsAccountVerified == true) {
                vm.isVerifiedBankAccount = true;
                vm.isVerifyBankAccount = false;
            } else {
                vm.isVerifiedBankAccount = false;
                vm.isVerifyBankAccount = true;
            }

            if (!angular.isUndefinedOrNull(vm.profileExist) && !angular.isUndefinedOrNull(vm.profileExist.IsIdVerified) && vm.profileExist.IsIdVerified == true) {
                vm.saIdNotVerified = false;
                vm.saIdVerified = true;
            } else {
                vm.saIdNotVerified = true;
                vm.saIdVerified = false;
            }

            if (!angular.isUndefinedOrNull(vm.profileExist) && !angular.isUndefinedOrNull(vm.profileExist.IsEmailVerified) && vm.profileExist.IsEmailVerified == true) {
                vm.emailNotVerified = false;
                vm.emailVerified = true;
            } else {
                vm.emailNotVerified = true;
                vm.emailVerified = false;
            }

            vm.profileExist.IsCellNumberVerified = angular.isUndefinedOrNull(vm.profileExist.IsCellNumberVerified) ? false : vm.profileExist.IsCellNumberVerified;
            vm.profileExist.IsEmailVerified = angular.isUndefinedOrNull(vm.profileExist.IsEmailVerified) ? false : vm.profileExist.IsEmailVerified;
            vm.profileExist.IsAccountVerified = angular.isUndefinedOrNull(vm.profileExist.IsAccountVerified) ? false : vm.profileExist.IsAccountVerified;
        }

        function RewardPtsUpdate() {
            // editProfileService.getTotalRewardPoints($localStorage.userDetails.C_MatterID).then(function (response) {
            editProfileService.getStatus($localStorage.userDetails.C_MatterID).then(function (response) {
                if (response != null) {
                    vm.profile.TotalRewardPoints = response.TotalRewardPoints;
                    vm.profile.NextStatus = response.ProfileNextStatus,
                    vm.profile.ProfileStatus = response.ProfileCurrentStatus;
                    // $localStorage.userDetails.ProfileStatus = vm.profile.ProfileStatus;
                    //$rootScope.ProfileStatus = response.ProfileStatusTypeName;
                    var imagePath = "images/edit_profile/u157_" + response.ProfileCurrentStatus + ".png";
                    vm.StatusImage = imagePath;
                    // vm.profile.ProfileStatus = response.ProfileStatusTypeName;
                    $localStorage.ProfileStatusTypeName = response.ProfileCurrentStatus;
                    $localStorage.profileImage = vm.StatusImage;
                    vm.profile.Name = angular.isUndefinedOrNull(vm.profile.Name) ? $localStorage.userDetails.D_FullNames : vm.profile.Name;
                    vm.profileExist.Name = angular.isUndefinedOrNull(vm.profileExist.Name) ? $localStorage.userDetails.D_FullNames : vm.profile.Name;


                    vm.profile.Surname = angular.isUndefinedOrNull(vm.profile.Surname) ? $localStorage.userDetails.D_Surname : vm.profile.Surname;

                    vm.profileExist.Surname = angular.isUndefinedOrNull(vm.profileExist.Surname) ? $localStorage.userDetails.D_Surname : vm.profile.Surname;

                }
                else {
                    editProfileService.getStatus($localStorage.userDetails.C_MatterID).then(function (response) {
                        if (response != null) {
                            vm.profile.TotalRewardPoints = response.TotalRewardPoints;
                            vm.profile.NextStatus = response.ProfileNextStatus,
                            vm.profile.ProfileStatus = response.ProfileCurrentStatus;

                            $localStorage.userDetails.ProfileStatus = vm.profile.ProfileStatus;
                            $rootScope.ProfileStatus = vm.profile.ProfileStatus;
                            var imagePath = "images/edit_profile/u157_" + $rootScope.ProfileStatus + ".png";
                            vm.StatusImage = imagePath;

                            $localStorage.profileImage = vm.StatusImage;
                            $localStorage.ProfileStatusTypeName = response.ProfileCurrentStatus;
                            vm.profile.Name = angular.isUndefinedOrNull(vm.profile.Name) ? $localStorage.userDetails.D_FullNames : vm.profile.Name;
                            vm.profileExist.Name = angular.isUndefinedOrNull(vm.profileExist.Name) ? $localStorage.userDetails.D_FullNames : vm.profile.Name;


                            vm.profile.Surname = angular.isUndefinedOrNull(vm.profile.Surname) ? $localStorage.userDetails.D_Surname : vm.profile.Surname;

                            vm.profileExist.Surname = angular.isUndefinedOrNull(vm.profileExist.Surname) ? $localStorage.userDetails.D_Surname : vm.profile.Surname;


                        }
                    });
                }
            });
        }

        function preferableContactMethod() {
            if (vm.profile.preferableContactMethod != null) {
                if (vm.profile.PreferableContactMethod == enumService.PreferableContactMethod.Call)
                    vm.profileExist.PreferableContactMethod = applicationSettings.PreferableContactMethod.Call;
                else if (vm.profile.PreferableContactMethod == enumService.PreferableContactMethod.SMS)
                    vm.profileExist.PreferableContactMethod = applicationSettings.PreferableContactMethod.SMS;
                else if (vm.profile.PreferableContactMethod == enumService.PreferableContactMethod.Email)
                    vm.profileExist.PreferableContactMethod = applicationSettings.PreferableContactMethod.Email;
                else if (vm.profile.PreferableContactMethod == enumService.PreferableContactMethod.Whatsapp)
                    vm.profileExist.PreferableContactMethod = applicationSettings.PreferableContactMethod.Whatsapp;
            }
        }

        vm.navigateDashBoard = function () {
            navigation.goToDashboard();
            $rootScope.header = "My DashBoard";
        }

        //Save Profile Details
        vm.save = function (profile, form) {
            if (angular.isUndefinedOrNull($localStorage.userDetails) && angular.isUndefinedOrNull($localStorage.userDetails.C_MatterID))
                return navigation.goToLogin();
            else if (angular.isUndefinedOrNull(profile.Initials))
                return log.error("Error", "Please Enter Initials");
            else if (angular.isUndefinedOrNull(profile.Name))
                return log.error("Error", "Please Enter Name");
            else if (angular.isUndefinedOrNull(profile.Surname))
                return log.error("Error", "Please Enter Surname");
            else if (angular.isUndefinedOrNull(profile.SA_ID))
                return log.error("Error", "Please Enter ID Number");

            messagingService.broadcastCheckFormValidatity();

            profile.gender = parseInt(profile.gender);
            profile.Matter_Id = $localStorage.userDetails.C_MatterID;
            profile.DebtorIdentityNo = angular.isUndefinedOrNull($localStorage.userDetails.C_DebtorIdentityNo) ? null : $localStorage.userDetails.C_DebtorIdentityNo;
            vm.rewardPtDetail.Matter_Id = $localStorage.userDetails.C_MatterID;
            vm.rewardPtDetail.CreationUserId = profile.Name;

            if (!angular.isUndefinedOrNull(profile.BranchCode) && profile.BranchCode == 7 && angular.isUndefinedOrNull(profile.BranchCodeOther))
                return log.error("Error", "Please Enter Branchcode for Other");
            else
                profile.BranchCodeOther == null;

            emailValidCheck();
            cellNumberValidCheck();
            bankAccountValidCheck();

            editProfileService.save(profile).then(function (response) {
                if (response != null) {
                    var imagePath = "images/edit_profile/u157_" + response.ProfileStatusTypeName + ".png";
                    vm.StatusImage = imagePath;
                    $localStorage.ProfileStatus = response.ProfileStatusTypeName;
                    $localStorage.profileImage = vm.StatusImage;
                    RewardPtsUpdate();
                    //vm.profile.TotalRewardPoints = response.TotalRewardPoints;
                    //vm.profile.ProfileStatus = response.ProfileStatusTypeName;
                    angular.copy(response, $rootScope.ProfileInfo);
                    angular.copy(response, vm.profileExist);
                    VerifiedIconEnableDissable();
                    preferableContactMethod();
                }
                return log.success("Success", "Saved Successfully");
            },
            function (err) {
                if (!angular.isUndefined(err.data)) {
                    if (!angular.isUndefined(err.data.errorList))
                        vm.validationErrors = err.data.errorList;
                }
            });
        }

        vm.update = function (addform, addprofile) {
            editProfileService.update(addprofile).then(function (response) {
                if (response != null && response.Result)
                    navigation.goToPaymentCompleted();
            },
            function (err) {
                if (!angular.isUndefined(err.data))
                    vm.validationErrors = err.data.errorList;
            });
        }

        //Verify Bank Account
        vm.verify = function (form, profile) {
            if (angular.isUndefinedOrNull($localStorage.userDetails) && angular.isUndefinedOrNull($localStorage.userDetails.C_MatterID))
                return navigation.goToLogin();
            else if (angular.isUndefinedOrNull(profile.Initials))
                return log.error("Error", "Please Enter Initials");
            else if (angular.isUndefinedOrNull(profile.Name))
                return log.error("Error", "Please Enter Name");
            else if (angular.isUndefinedOrNull(profile.Surname))
                return log.error("Error", "Please Enter Surname");
            else if (angular.isUndefinedOrNull(profile.SA_ID))
                return log.error("Error", "Please Enter ID Number");

            if (!angular.isUndefinedOrNull(profile.AccountNumber) &&
                !angular.isUndefinedOrNull(profile.Bank) &&
                !angular.isUndefinedOrNull(profile.BranchCode)) {

                if (profile.BranchCode == 7 && (angular.isUndefinedOrNull(profile.BranchCodeOther)))
                    return log.error("Error", "Please Enter Branchcode for Other");
                else if (profile.BranchCode != 7)
                    profile.BranchCodeOther == null;

                vm.isInValidBankDetails = true;
                vm.profile.Matter_Id = $localStorage.userDetails.C_MatterID;
                vm.profile.DebtorIdentityNo = angular.isUndefinedOrNull($localStorage.userDetails.C_DebtorIdentityNo) ? null : $localStorage.userDetails.C_DebtorIdentityNo;

                emailValidCheck();
                cellNumberValidCheck();
                // Show the Modal on load
                $("#myModalBank").modal("show");

                editProfileService.verifyBankAccount(profile).then(function (response) {
                    vm.isVerifyBankAccount = false;
                    vm.isVerifiedBankAccount = false;

                    if ((response.errorList == null || response.errorList != null) && response.Result == false) {
                        vm.isVerifiedBankAccount = false;
                        vm.isVerifyBankAccount = true;
                        vm.profile.IsAccountVerified = false;
                        if (angular.isUndefinedOrNull(profile.SA_ID))
                            return log.error("Error", "Please Enter ID Number");
                        else if (profile.SA_ID.length < 13)
                            return log.error("Error", "ID Number must be made up of 13 digits");
                        else if (angular.isUndefinedOrNull(profile.Bank))
                            return log.error("Error", "Please Enter Bank");
                        else if (angular.isUndefinedOrNull(profile.BranchCode) || profile.BranchCode == 0)
                            return log.error("Error", "Please Enter BranchCode");
                        else if (angular.isUndefinedOrNull(profile.AccountNumber))
                            return log.error("Error", "Please Enter Account Number");
                        //else if (profile.AccountNumber.length < 13)
                        //    return log.error("Error", "Account number needs to be 1 to 13 digits long");
                    }
                    else {
                        vm.isVerifyBankAccount = false;
                        vm.isVerifiedBankAccount = true;
                        vm.profile.IsAccountVerified = true;
                        vm.profileExist.Bank = vm.profile.Bank;
                        vm.profileExist.BranchCode = vm.profile.BranchCode;
                        vm.profileExist.BranchName = vm.profile.BranchName;
                        vm.profileExist.AccountNumber = vm.profile.AccountNumber;
                        RewardPtsUpdate();
                    }
                },
                function (err) {
                    if (!angular.isUndefined(err.data))
                        vm.validationErrors = err.data.errorList;
                });
            }
            else {
                vm.isInValidBankDetails = false;
                if (angular.isUndefinedOrNull(profile.SA_ID))
                    return log.error("Error", "Please Enter ID Number");
                else if (profile.SA_ID.length < 13)
                    return log.error("Error", "ID Number must be made up of 13 digits");
                else if (angular.isUndefinedOrNull(profile.Bank))
                    return log.error("Error", "Please Enter Bank");
                else if (angular.isUndefinedOrNull(profile.BranchCode) || profile.BranchCode == 0)
                    return log.error("Error", "Please Enter BranchCode");
                else if (angular.isUndefinedOrNull(profile.AccountNumber))
                    return log.error("Error", "Please Enter Account Number");
                //else if (profile.AccountNumber.length < 13)
                //    return log.error("Error", "Account number needs to be 1 to 13 digits long");
            }
        }

        //Email Verify
        vm.verifyEmail = function (form, profile) {
            if (angular.isUndefinedOrNull($localStorage.userDetails) && angular.isUndefinedOrNull($localStorage.userDetails.C_MatterID))
                return navigation.goToLogin();
            else if (angular.isUndefinedOrNull(profile.Initials))
                return log.error("Error", "Please Enter Initials");
            else if (angular.isUndefinedOrNull(profile.Name))
                return log.error("Error", "Please Enter Name");
            else if (angular.isUndefinedOrNull(profile.Surname))
                return log.error("Error", "Please Enter Surname");

            else if (angular.isUndefinedOrNull(profile.Email))
                return log.error("Error", "Please Enter Email");

            //if (!form.$invalid) {
            // Show the Modal on load              


            $("#myModal").modal("show");

            vm.emailVerified = true;
            vm.emailNotVerified = false;
            var verifyEmailData = {
                ClientUrl: environment.clientBaseUrl,
                EmailId: profile.Email,
                Name: profile.Name,
                MatterId: $localStorage.userDetails.C_MatterID,
                SurName: profile.Surname,
                Title: profile.title
            };

            editProfileService.verifyEmail(verifyEmailData).then(function (response) {
                if (response != null && response.Result) {
                    navigation.goToPaymentCompleted();
                    vm.emailVerified = true;
                    vm.emailNotVerified = false;
                    RewardPtsUpdate();
                }
                else {
                    vm.emailVerified = false;
                    vm.emailNotVerified = true;
                }
            },
            function (err) {
                if (!angular.isUndefinedOrNull(err.data))
                    vm.validationErrors = err.data.errorList;
                //else
                //    return log.error("Error", "Requested Payment Failed");
            });
            //}
        }

        //SMS Verify
        vm.verifySms = function (form, profile) {
            vm.oTP = null;
            if (angular.isUndefinedOrNull(profile.Initials))
                return log.error("Error", "Please Enter Initials");
            else if (angular.isUndefinedOrNull(profile.Name))
                return log.error("Error", "Please Enter Name");
            else if (angular.isUndefinedOrNull(profile.Surname))
                return log.error("Error", "Please Enter Surname");
            else if (angular.isUndefinedOrNull(profile.SA_ID))
                return log.error("Error", "Please Enter ID Number");
            else if (angular.isUndefinedOrNull(profile.CellNumber))
                return log.error("Error", "Please Enter Cell Number");

            //Timer Variables
            var threeMinutes = 60 * 3,
                        display = document.querySelector('#otpTimer');

            if (prevNowPlaying)
                clearInterval(prevNowPlaying);

            startTimer(threeMinutes, display);

            emailValidCheck();
            bankAccountValidCheck();

            //$("#myModalCell").modal("show");
            $('#myModalCell').modal({
                keyboard: false,
                show: true,
            });

            editProfileService.verifySms(profile).then(function (response) {
                if (response != null && response.Result)
                    navigation.goToProfile();

            },
            function (err) {
                if (!angular.isUndefinedOrNull(err.data))
                    vm.validationErrors = err.data.errorList;
            });
        }


        function startTimer(duration, display) {
            var start = Date.now(),
                diff,
                minutes,
                seconds;

            function timer() {
                // get the number of seconds that have elapsed since 
                // startTimer() was called                       

                diff = duration - (((Date.now() - start) / 1000) | 0);

                // does the same job as parseInt truncates the float
                minutes = (diff / 60) | 0;
                seconds = (diff % 60) | 0;

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.textContent = minutes + ":" + seconds;

                if (diff <= 0) {
                    // add one second so that the count down starts at the full duration
                    // example 05:00 not 04:59
                    start = Date.now() + 1000;
                    $("#myModalCell").modal("hide");
                }
            };
            // we don't want to wait a full second before the timer starts
            timer();
            prevNowPlaying = setInterval(timer, 1000);
        };


        vm.cancelTimer = function () {
            var display = angular.element(document.querySelector('#time'));
            display.context.textContent = null;
        }

        vm.verifyOTP = function (otp) {
            if (angular.isUndefinedOrNull($localStorage.userDetails) && angular.isUndefinedOrNull($localStorage.userDetails.C_MatterID))
                return navigation.goToLogin();
            else if (angular.isUndefinedOrNull(otp))
                return log.error("Error", "Please Enter OTP");

            emailValidCheck();
            bankAccountValidCheck();

            vm.OTPVerified = false;
            vm.OTPNotVerified = false;
            vm.profile.Matter_Id = $localStorage.userDetails.C_MatterID;
            vm.profile.DebtorIdentityNo = angular.isUndefinedOrNull($localStorage.userDetails.C_DebtorIdentityNo) ? null : $localStorage.userDetails.C_DebtorIdentityNo;
            vm.profile.oTP = otp;
            editProfileService.otpCheck(vm.profile).then(function (response) {
                if ((response != null) && (response.Result.IsCellNumberVerified == true)) {
                    vm.otpVerified = true;
                    vm.otpNotVerified = false;
                    vm.profile.IsCellNumberVerified = true;
                    vm.profileExist.IsCellNumberVerified = true;
                    vm.profileExist.CellNumber = vm.profile.CellNumber;
                    RewardPtsUpdate();
                }
                else {
                    vm.otpNotVerified = true;
                    vm.otpVerified = false;
                    vm.profile.IsCellNumberVerified = false;

                }
                $("#myModalCell").modal("hide");
            },
            function (err) {
                if (!angular.isUndefinedOrNull(err.data)) {
                    vm.profile.IsCellNumberVerified = false;
                    $("#myModalCell").modal("hide");
                    vm.validationErrors = err.data.errorList;
                }
            });
        }

        vm.RewardDecTypeExists = function (rewardPtDetail, rewardPtDecId) {

            rewardPtDetail.RewardPointDescType = rewardPtDecId;
            editProfileService.verifyRewardPts(rewardPtDetail).then(function (response) {

                vm.response.ID = response.ID;
            },
                            function (err) {
                                if (!angular.isUndefined(err.data))
                                    vm.validationErrors = err.data.errorList;
                                //else
                                //    return log.error("Error", "Requested Payment Failed");
                            });
        }

        $scope.removeTagOnBackspace = function (event) {
            if (event.keyCode === 8) {
                //console.log('here!');
                //event.preventDefault();
                return true;
            }
        };



        $('.launchmodal').on('click', function (e) {
            $('#myModal').modal({
                keyboard: false,
                show: true,
            });
        });




        //Branch Type load
        function activate() {

            //editProfileService.getPreferableContactMethod().then(function (res) {

            //});
            $q.all([
                    paymentService.getBranchType(vm.refresh),
                    paymentService.getTitle(vm.refresh),
                                        editProfileService.getProfileDetails($localStorage.userDetails.C_MatterID),
                                         editProfileService.getTotalRewardPoint($localStorage.userDetails.C_MatterID),
                                         editProfileService.getPreferableContactMethod(vm.refresh)
            ]).then(function (responses) {
                vm.branch = responses[0];
                vm.title = responses[1];
                vm.profile = responses[2];
                vm.preferableContactMethod = responses[4];

                if (!angular.isUndefinedOrNull(vm.profile) && vm.profile.ID > 0) {
                    if (!angular.isUndefinedOrNull(vm.profile.JobStartDate))
                        vm.profile.JobStartDate = moment(vm.profile.JobStartDate).format("DD/MM/YYYY");
                    else
                        vm.profile.JobStartDate = "";

                    vm.profile.title = $linq.Enumerable().From(vm.title).Where(function (x) {
                        return x.Key == responses[2].TitleType;
                    }).Select(function (x) {
                        return x.Key
                    }).FirstOrDefault();

                    vm.profile.branch = $linq.Enumerable().From(vm.branch).Where(function (x) {
                        return x.Value == responses[2].BranchCode;
                    }).Select(function (x) {
                        return x.Key
                    }).FirstOrDefault();

                    //vm.profile.PreferableContactMethod = $linq.Enumerable().From(vm.preferableContactMethod).Where(function (x) {
                    //    return x.Value == responses[2].PreferableContactMethod;
                    //}).Select(function (x) {
                    //    return x.Key
                    //}).FirstOrDefault();


                }
                else {
                    //vm.profile = {};
                    vm.profile.title = enumService.TitleType.Mr;
                    vm.profile.Gender = 1;
                    vm.profile.ID = 0;
                    vm.profile.Name = responses[2].Name;
                    vm.profile.Surname = responses[2].Surname;

                    //vm.profile.PreferableContactMethod = 1;
                    //if (vm.profile.PreferableContactMethod == 1)
                    // vm.profile.PreferableContactMethod=enumService.PreferableContactMethod.SMS;


                    //vm.profile.Gender = angular.isUndefinedOrNull(vm.profile.Gender) ? $localStorage.userDetails.Gender : vm.profile.Gender;
                    //vm.profileExist.Gender = angular.isUndefinedOrNull(vm.profileExist.Gender) ? $localStorage.userDetails.Gender : vm.profile.Gender;

                }

                if (vm.profile.JobStartDate == null)
                    vm.profile.JobStartDate = "";

                angular.copy(vm.profile, vm.profileExist);
                angular.copy(vm.profile, $rootScope.ProfileInfo);

                preferableContactMethod();
                vm.profile.TotalRewardPoints = responses[3];
                vm.profile.PreferableContactTime = responses[2].PreferableContactTime;
                preferableContactMethod();
                VerifiedIconEnableDissable();
                RewardPtsUpdate();

            });

        }


        vm.dateList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];


        $('#PreferableContactTimeFrom').timepicker().on('changeTime.timepicker', function (e) {
            vm.profile.PreferableContactTimeFrom = e.time.value;
        });

        $('#PreferableContactTimeTo').timepicker().on('changeTime.timepicker', function (e) {
            vm.profile.PreferableContactTimeTo = e.time.value;
        });

        activate();

        vm.getTemplate = function () {
            var Id = document.getElementById("BranchCodeId");
            var value = Id.selectedOptions[0].text;
            var values = value.split("-");
            var BankValue = values[1];
            vm.profile.Bank = BankValue;

        };

        vm.gettempalateByPreferableContactMethod = function () {
            var Id = document.getElementById("BranchCodeId");
            var value = Id.selectedOptions[0].text;
            vm.profile.PreferableContactMethod = value;
        }

        setTimeout(function () { signalrListener(); }, 3000);
        return vm;
    }
})();
