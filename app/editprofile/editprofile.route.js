﻿(function () {
    'use strict';
    angular
        .module('app.editprofile')
        .run(appRun);
    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {

                state: 'dashboard.editprofile',
                config: {
                    url: 'editprofile',
                    templateUrl: '/app/editprofile/editprofile.html',
                    controller: 'EditProfileController',
                    controllerAs: 'vm',
                    title: 'EditProfile',
                    ncyBreadcrumb: { label: 'EditProfile' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();