﻿(function () {
    'use strict';

    angular
        .module('app.notification')
        .controller('NotificationController', controller);

    controller.$inject = ['session', 'notificationService', 'EnumService', '$q', '$linq', 'navigation', '$interval', 'modalService', '$modal', 'environment', '$localStorage', '$rootScope'];
    /* @ngInject */
    function controller(session, notificationService, enumService, $q, $linq, navigation, $interval, modalService, $modal, environment, $localStorage, $rootScope) {
        $rootScope.layoutHeaderChanges = null;
        var vm = this;
        $rootScope.header = "Notification";
        angular.isUndefinedOrNullOrEmpty = function (val) {
            return angular.isUndefined(val) || val === null || val === "";
        }
        function notificationList() {
            notificationService.getNotification()
                     .then(function (results) {
                         vm.notificationList = results;
                     });
        }       

        vm.search = function () {
            var postData = {};
            if (!angular.isUndefinedOrNullOrEmpty(vm.searchText)) {
                postData.searchQuery = vm.searchText;
                notificationService.genotificationSearchList(postData).then(function (response) {
                    vm.notificationList = response;
                });
            }
        }
        vm.refresh = function () {
            notificationList();
            vm.searchText = "";
        }
        function activate() {
            notificationList();
        }
        activate();
        return vm;
    }
})();