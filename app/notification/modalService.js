﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('modalService', service);

    service.$inject = ['$modal'];

    function service($modal) {

        var svc = {};

        //prevent the backspace navigation issue, when user hit backspace
        //page nagvigates to previous screen
        $(document).bind('keydown', function (e) {
            if (e.which === 8 && (e.target.className.indexOf("modal") > -1 || e.target.parentNode.className.indexOf("modal") > -1)) {
                e.preventDefault();
            }
        });

        svc.openPdfReportModal = function (reportname, pdfurl) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/pdfReportModal.html',
                controller: 'PdfReportModalController',
                controllerAs: 'vm',
                size: 'lg',
                windowClass: 'model-zindex-withouttop',
                backdrop: 'static',
                resolve: {
                    reportName: function () {
                        return reportname;
                    },
                    pdfUrl: function () {
                        return pdfurl;
                    }
                }
            });
        };

        svc.confirmDelete = function (confirmationMessage) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/genericDeleteConfirmationModal.html',
                controller: 'GenericDeleteConfirmationModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static',
                resolve: {
                    confirmationMessage: function () {
                        return confirmationMessage;
                    }
                }
            });
        };

        svc.questionModal = function (title, message, includeDangerHeader, item) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/genericQuestionModal.html',
                controller: 'GenericQuestionModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static',
                resolve: {
                    title: function () {
                        return title;
                    },
                    item: function () {
                        return item;
                    },
                    message: function () {
                        return message;
                    },
                    includeDangerHeader: function () {
                        return includeDangerHeader;
                    }
                }
            });
        };

        svc.serviceQuestionModal = function (title, message, includeDangerHeader) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/serviceQuestionModel.html',
                controller: 'GenericQuestionModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static',
                resolve: {
                    title: function () {
                        return title;
                    },
                    message: function () {
                        return message;
                    },
                    includeDangerHeader: function () {
                        return includeDangerHeader;
                    }
                }
            });
        };

        //forgotpasswordModel
        svc.forgotpassword = function () {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/forgotpasswordModel.html',
                controller: 'ForgotModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static'

            });
        };

        svc.changePassword = function () {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/changepasswordModal.html',
                controller: 'ChangePasswordModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static'

            });
        };

        svc.messageModal = function (title, message, titleClass) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/genericMessageModal.html',
                controller: 'GenericMessageModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static',
                resolve: {
                    title: function () {
                        return title;
                    },
                    message: function () {
                        return message;
                    },
                    titleClass: function () {
                        return titleClass;
                    }
                }
            });
        };

        svc.termsAndConditionModal = function (title, message) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/termsAndConditionModal.html',
                controller: 'TermsAndConditionModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-terms',
                backdrop: 'static',
                resolve: {
                    title: function () {
                        return title;
                    },
                    message: function () {
                        return message;
                    }
                }
            });
        };

        svc.requestLoginModal = function () {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/AccountVerifyModal.html',
                controller: 'AccountVerifyModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static'
                //resolve: {
                //    title: function () {
                //        return title;
                //    },
                //    message: function () {
                //        return message;
                //    }
                //}
            });
        };


        svc.debitFormPrintModal = function (encodedDebitFormDetails) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/checkcoverage/debitform.printtemplate.html',
                controller: 'debitFormPrintController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-terms',
                backdrop: 'static',
                resolve: {
                    encodedDebitFormDetails: function () {
                        return encodedDebitFormDetails;
                    },
                }
            });
        };

        svc.rejectioMmessageModal = function (title, message, titleClass, item) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/rejectionMessageModal.html',
                controller: 'RejectionMessageModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static',
                resolve: {
                    title: function () {
                        return title;
                    },
                    message: function () {
                        return message;
                    },
                    titleClass: function () {
                        return titleClass;
                    },
                    item: function () {
                        return item;
                    }
                }
            });
        };

        svc.closeSupportModal = function (title, message, titleClass, item) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/closeSupportModal.html',
                controller: 'CloseSupportModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static',
                resolve: {
                    title: function () {
                        return title;
                    },
                    message: function () {
                        return message;
                    },
                    titleClass: function () {
                        return titleClass;
                    },
                    item: function () {
                        return item;
                    }
                }
            });
        };

        svc.warrningMessageModel = function (title, message, includeDangerHeader) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/warnningMessageModel.html',
                controller: 'WarnningMessageModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static',
                resolve: {
                    title: function () {
                        return title;
                    },
                    message: function () {
                        return message;
                    },
                    includeDangerHeader: function () {
                        return includeDangerHeader;
                    }
                }
            });
        };

        svc.customdaterange = function (item) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/layout/CustomDateRange.html',
                controller: 'CustomerDateRangeModalController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });

        };

        svc.paymentMessage = function (TotalRewardPoints) {
            return $modal.open({
                animation: true,
                templateUrl: 'app/payment/paymentmessage.html',
                controller: 'paymentMessageController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'model-zindex',
                backdrop: 'static',
                resolve: {
                    TotalRewardPoints: function () {
                        return TotalRewardPoints;
                    }
                }
            });
        };
        return svc;
    }

})();
