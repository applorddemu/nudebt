﻿(function () {
    'use strict';

    angular
        .module('app.notification')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.notification',
                config: {
                    url: 'notification',
                    templateUrl: '/app/notification/notification.html',
                    controller: 'NotificationController',
                    controllerAs: 'vm',
                    title: 'Notification',
                    ncyBreadcrumb: { label: 'Notification' },
                    settings: {
                        mustBeAuthenticated: false
                    }
                }
            }
        ];
    }
})();