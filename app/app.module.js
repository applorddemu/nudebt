(function () {
    'use strict';

    angular.module('app', [
        'app.core',
        'app.dashboard',
        'app.login',
        'app.layout',
        'app.payment',
        'app.breakdown',
        'app.notification',
        'app.paymentlog',
        'app.editprofile',
        'ngSanitize',
        'ngStorage',
        'smart-table',
        'ae-datetimepicker',
        'easypiechart',
        //'chart.js'
        //'kendo.directives',
    ]);

})();
