﻿(function () {
    'use strict';

    angular
        .module('app.breakdown')
        .controller('BreakDownController', controller);

    controller.$inject = ['authService', 'navigation', '$q', 'breakdownService', '$linq', 'id', '$localStorage', '$rootScope', '$scope'];
    /* @ngInject */
    function controller(authService, navigation, $q, breakdownService, $linq, id, $localStorage, $rootScope, $scope) {
        var vm = this;
        vm.breakdownid = id;
        vm.CreditorName = "";
        vm.CreditorAddress = "";
        vm.MatterNumber = "";
        $rootScope.header = "Balance Breakdown";
        $rootScope.isBreakDown = false;
        vm.MonthList;
        vm.outstanding;
        $scope.outstanding;
        $rootScope.percent;
        $scope.capitalAmt;

        $scope.options = {
            animate: {
                duration: 0,
                enabled: false
            },
            barColor: '#FFCE58',
            trackColor: '#f5f5f5',
            scaleColor: false,
            lineWidth: 15,
            lineCap: 'square',
            size: '200'
        };


        //$scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
        $scope.series = ['Outstanding %'];
        $scope.data = [

        ];
        //$scope.onClick = function (points, evt) {
        //    console.log(points, evt);
        //};

        $scope.colors = ['#FFCE58'];
        // $scope.colors = [{ backgroundColor: ['#FFCE58','#3852a3']}];
        $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];

        $scope.optionsLine = {
            scales: {

                yAxes: [
                  {
                      id: 'y-axis-1',
                      type: 'linear',
                      display: true,
                      position: 'left',
                      scaleStartValue: 0,
                      scaleLabel: {
                          display: true,
                          labelString: 'Outstanding Percentage'
                      },
                      ticks: {
                          beginAtZero: true,
                          stepSize: 25,
                          stepValue: 25,
                          max: 100
                      },

                  },
                  //{
                  //    id: 'y-axis-2',
                  //    type: 'linear',
                  //    display: true,
                  //    position: 'right',
                  //    scaleStartValue: 0,
                  //    ticks: {
                  //        beginAtZero: true,
                  //        max: vm.capitalAmt
                  //    },
                  //    scaleLabel: {
                  //        display: true,
                  //        labelString: 'Outstanding Amount'
                  //    },

                  //}
                ],
                pointDot: true,
                pointDotRadius: 3,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 50,
                pointHighlightFill: "#3852a3",
            },

        };

        //vm.breakdownpiechartlabels = ["Capital", "dfsd"];
        //vm.breakdownpiechartdata = [412, 65];
        vm.getCapitalAmtDetails = function () {
            breakdownService.getCapitalAmtDetails(vm.breakdownid)
                       .then(function (results) {
                           if (results != null) {
                               vm.MatterNumber = results.M_ID;
                               vm.capitalAmt = results.M_CapitalAmt.toFixed(2);
                               $scope.capitalAmt = vm.capitalAmt;
                               vm.M_OutstandingAmt = results.M_OutstandingAmt;
                               vm.M_CreditorName = results.M_CreditorName;
                               vm.getBreakdownList(results.M_ID, results.M_IDX);
                               vm.getPreviousPaymentList(results.M_ID, results.M_IDX);
                               vm.GetPreviousPaymentChartList(results.M_ID, results.M_IDX);
                               //vm.paidAmt = (results.M_CapitalAmt - results.M_OutstandingAmt).toFixed(2);
                               //vm.percent = Math.abs(((vm.paidAmt/results.M_CapitalAmt ) * 100).toFixed(0));
                           }
                       });
        }
        vm.getBreakdownList = function (matterId, matterIndexId) {

            breakdownService.getBreakdownList(matterId, matterIndexId)
                       .then(function (results) {
                           if (results != null) {
                               vm.breakdownList = results;
                               var paymentAddressDetail = $linq.Enumerable().From(results).Select(function (x) {
                                   return x
                               }).FirstOrDefault();

                               if (paymentAddressDetail != null && paymentAddressDetail != undefined) {
                                   vm.CreditorName = paymentAddressDetail.CreditorName;
                                   vm.CreditorAddress = paymentAddressDetail.CreditorAddress;
                                   vm.CreditorDate = paymentAddressDetail.Date;

                                   vm.BreakdownTotal = paymentAddressDetail.Total;
                               }

                               vm.breakdownpiechartlabels = $linq.Enumerable().From(results)
                                                                   .Select(function (x) {
                                                                       return x.Capital
                                                                   })
                                                                   .ToArray();
                               //vm.breakdownpiechartdata = $linq.Enumerable().From(results)
                               //                        .Select(function (x) {
                               //                            return x.CapitalAmt
                               //                        })
                               //                        .ToArray();
                               var option = {
                                   rotation: 1 * Math.PI,
                                   circumference: 1 * Math.PI
                               };
                               vm.chartoption = option;
                           }
                       });
        }
        vm.getPreviousPaymentList = function (matterId, matterIndexId) {
            breakdownService.getPreviousPaymentList(matterId, matterIndexId)
                       .then(function (results) {
                           if (results != null) {
                               vm.previousPaymentList = results;
                               var previousPaymentDetails = $linq.Enumerable().From(results).Select(function (x) {
                                   return x
                               }).FirstOrDefault();

                               angular.forEach(vm.previousPaymentList, function (item, value) {
                                   if (item != null) {
                                       item.Amount = numberWithCommas(item.Amount);
                                       item.Balance = numberWithCommas(item.Balance);
                                   }
                               });
                               if (previousPaymentDetails != null && previousPaymentDetails != undefined) {
                                   //vm.PreviousPaymentTotal = format1(previousPaymentDetails.Total);
                                   vm.PreviousPaymentTotal = numberWithCommas(previousPaymentDetails.Total);                                   
                               }
                           }
                       });
        }


        //var formatter = new Intl.NumberFormat('af-ZA', {
        //    style: 'currency',
        //    currency: 'ZAR',
        //    minimumFractionDigits: 2,
        //});

        function numberWithCommas(unForNum) {
            var unDeciVal = parseFloat(unForNum).toFixed(2);
            var parts = unDeciVal.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            //console.log(parts[0]);
            //var forNum = Math.abs(Number(parts.join(".")));            
            return parts.join(".");
        }
        


        vm.GetPreviousPaymentChartList = function (matterId, matterIndexId) {
            breakdownService.getPreviousPaymentChartDetail(matterId, matterIndexId)
                       .then(function (results) {
                           if (results.length != 0) {
                               vm.MonthList = results[0].monYear;
                               vm.PercentList = results[0].outstandingpercent;
                               vm.outstanding = results[0].balanceamt;
                               vm.outstandingAmount = results[0].outstandingamount,
                               $scope.data = [vm.PercentList];

                               $scope.labels = vm.MonthList;
                               $scope.outstanding = vm.outstanding;
                               vm.paidAmt = (vm.capitalAmt - vm.outstanding).toFixed(2);
                               vm.percent = Math.abs(((vm.paidAmt / vm.capitalAmt) * 100).toFixed(1));
                               vm.outstanding = numberWithCommas(vm.outstanding);
                               vm.capitalAmt = numberWithCommas(vm.capitalAmt);
                           }                           
                       });
        }



        function activate() {
            vm.getCapitalAmtDetails();
            //vm.getBreakdownList();
            //vm.getPreviousPaymentList();
        }
        activate();
        vm.paymentnavigate = function () {
            navigation.goToPayment(id);
            $rootScope.isBreakDown = true;

        }
        vm.navigateDashBoard = function () {
            navigation.goToDashboard();
            $rootScope.header = "My Dashboard";

        }
        return vm;
    }
})();