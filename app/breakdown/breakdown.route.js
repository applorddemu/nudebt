﻿(function () {
    'use strict';

    angular
        .module('app.breakdown')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.breakdown',
                config: {
                    url: 'breakdown/{id}',
                    templateUrl: '/app/breakdown/breakdown.html',
                    controller: 'BreakDownController',
                    controllerAs: 'vm',
                    title: 'BreakDown',
                    ncyBreadcrumb: { label: 'breakDown' },
                    settings: {
                        mustBeAuthenticated: true
                    },
                    
                    resolve: {
                        id: ['$stateParams', function ($stateParams) {
                            return $stateParams.id;
                        }]
                    },
                }
            }
        ];
   
    }
})();
