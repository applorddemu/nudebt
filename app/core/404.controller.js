﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('404Controller', controller);

    controller.$inject = ['$location'];
    /* @ngInject */
    function controller($location) {

        var vm = {};

        activate();

        function activate() {
            vm.currentLocation = $location.path();
        }

        return vm;

    }
})();