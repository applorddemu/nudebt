(function () {
    'use strict';

    angular
        .module('app.core', [
            'ngSanitize',
            'blocks.exception', 'blocks.logger', 'blocks.router',
            'ui.router', 'ngplus',
            //'ncy-angular-breadcrumb',
            'LocalStorageModule', 'ui.bootstrap', 'ui.bootstrap.tooltip', 'ui.bootstrap.showErrors',
            //'mgo-angular-wizard',
            'toaster', 'switcher', 'smart-table', 'ae-datetimepicker'
             //'kendo.directives',
        ]);
})();
