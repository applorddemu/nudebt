﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('InsufficientPermissionsController', controller);

    controller.$inject = ['SettingsDataService', 'InsufficientPermissionService'];
    /* @ngInject */
    function controller(SettingsDataService, InsufficientPermissionService) {

        var vm = {};

        vm.getData = function () {

            SettingsDataService.getCompanySettings()
                .then(function (result) {
                    vm.companyPrimaryEmail = result.primaryContactEmail;
                });

        };

        activate();

        function activate() {
            //vm.getData(); //THIS WAS REMOVED DUE TO THE CALL NEEDING PERMISSIONS.
            vm.screenName = InsufficientPermissionService.getScreenNameThatUserDoesntHavePermissionFor();
        }

        return vm;

    }
})();