﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('paymentlogService', service);

    service.$inject = ['dataService', 'environment', '$localStorage'];

    function service(dataService, environment, $localStorage) {

        var svc = {};
        svc.getPaymentLog = function () {
            return dataService.getRecord('api/paymentlog/getpaymentlog.php')
        }
        svc.getPaymentLogList = function (searchQuery) {
            return dataService.getRecordWithParams('api/paymentlog/GetPaymentLogList.php', searchQuery);
        }
        return svc;
    }

})();