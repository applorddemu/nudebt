﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('utilityService', service);

    service.$inject = ['environment', 'dataService'];

    function service(environment, dataService) {

        var svc = {};

        svc.getIpAddress = function () {
            return dataService.getRecord('/api/payment/GetIpAddress.php');
        }

        svc.getUserIpAddress = function() {
            return $.ajax({
                type: 'GET',
                url: 'https://freegeoip.net/json/' //'http://ipv4.myexternalip.com/json'
            });
        };

        return svc;
    }

})();
