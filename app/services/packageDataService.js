﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('packageDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};
        
        svc.GetSPAndProductListByCoordinates = function (paramFilters) {
            return dataService.getRecordWithParams('/api/coverage/GetSPAndProductListByCoordinates', paramFilters);
        };        

        return svc;
    }
})();