﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('log', service);

    service.$inject = ['toaster'];

    function service(toaster) {

        var svc = {};
 
        svc.error = function (title, message) {
            return toaster.pop({ type: 'error', title: title, body: message });
        };

        svc.success = function (title, message) {
            return toaster.pop({ type: 'success', title: title, body: message });
        };

        svc.info = function (title, message) {
            return toaster.pop({ type: 'info', title: title, body: message });
        };

        svc.warn = function (title, message) {
            return toaster.pop({ type: 'warning', title: title, body: message });
        };

        return svc;

    }

})();
