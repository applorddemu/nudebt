﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('lookupDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

       

        svc.clearCache = function () {
            return dataService.invalidateCache();
        }

        svc.gettitle = function (refresh) {
            return dataService.getLookupData('/api/lookup/gettitle', refresh);
        };
         
        svc.getlanguage = function (refresh) {
            return dataService.getLookupData('/api/lookup/getlanguage', refresh);
        }

        svc.getAccountType = function (refresh) {
            return dataService.getLookupData('/api/lookup/getAccountType', refresh);
        }

        svc.getCountryType = function (refresh) {
            return dataService.getLookupData('/api/lookup/getCountryType', refresh);
        }

        svc.getRoleType = function (refresh) {
            return dataService.getLookupData('/api/lookup/getRoleType', refresh);
        }
        svc.getMailCategory = function (refresh) {
            return dataService.getLookupData('/api/lookup/getMailCategory', refresh);
        }
       
        return svc;

    }

})();