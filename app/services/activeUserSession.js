﻿(function () {
    'use strict';

    angular
        .module('app')
        .service('activeUserSession', session);

    session.$inject = ['localStorageService', 'environment'];

    function session(localStorageService, env) {

        var localStorageSessionKey = 'NuDebtCollect-' + env.environment + '-ActiveUser';

        this.create = function (address, areaProductMappingId, productDetails) {// jshint ignore:line
            this.setLocalStorageProperties(address, areaProductMappingId, productDetails);
            this.setSessionProperties(address, areaProductMappingId, productDetails);
        };

        this.destroy = function () {// jshint ignore:line
            this.setLocalStorageProperties();
            this.setSessionProperties();
        };

        this.load = function () {// jshint ignore:line
            var localData = localStorageService.get(localStorageSessionKey);
            if (localData) {
                this.setSessionProperties(localData.address, localData.areaProductMappingId, localData.productDetails);
            }
        };

        this.setSessionProperties = function (address, areaProductMappingId, productDetails) { // jshint ignore:line
            this.address = address;
            this.areaProductMappingId = areaProductMappingId;
            this.productDetails = productDetails;
        }

        this.setLocalStorageProperties = function (address, areaProductMappingId, productDetails) {// jshint ignore:line
            localStorageService.set(localStorageSessionKey, {
                address: address,
                areaProductMappingId: areaProductMappingId,
                productDetails: productDetails,
            });
        };

        this.hasValue = function()
        {
            return this.address != null;

            //return this.address && this.provider && this.providerId && this.speedMbps;
        }
    }

})();
