(function() {
    'use strict';

    angular
        .module('app')
        .factory('authService', service);

    service.$inject = ['$rootScope', '$http', 'session', 'navigation', 'environment', 'activeUserSession', '$localStorage', 'dataService'];

    function service($rootScope, $http, session, navigation, env, activeUserSession, $localStorage, dataService) {

        var authService = {};
        var baseUrl = env.serverBaseUrl;

        authService.login = function (credentials) {
            return dataService.post('api/login/loginDetails', credentials)
      //      var url = baseUrl + "api/login/loginDetails?MatterNumer=" + credentials.matternumber + "&CellNumber=" + credentials.cellnumber + "&IDNumber=" + credentials.idnumber;        
         //  return $http.get(url);
            //return $http
            //    .post(url,
            //        //{ userName:"Test",password:"Test" },
            //        { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
            //    .then(function (res) {
            //        alert(credentials);
            //        $localStorage.userDetails = res.data;
            //        return res.data;
            //    });
        };

        authService.logOut = function () {
            $rootScope.layoutHeaderChanges = true;
            navigation.goToLogin();
        };

        authService.isAuthenticated = function () {
            return !!session.username && !!session.accessToken;
        };
        return authService;

    }

})();
