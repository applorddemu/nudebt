﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('paymentService', service);

    service.$inject = ['dataService', 'environment', '$localStorage'];

    function service(dataService, environment, $localStorage) {

        var svc = {};

        svc.save = function (payment) {
            return dataService.post('api/payment/savePayment', payment)
        }

        //svc.save = function (payment) {
        //    return dataService.post('api/ServiceController/ClientAVSSubmissionRequest', payment)
        //}

        svc.getZapperCode = function (zapper) {
            //http://2.zap.pe?t=6&i={MerchantId}:{SiteId}:7[34|{Amount}|{AmountType},{Tip},66|{MerchantR eference}|10,60|1:10[38|{ShortMerchantName},39|{CurrencyIsoCode}
            //return dataService.post('api/payment/savePayment/?MerchantId=' + zapper.merchantId + '&SiteId=' + zapper.siteId + '&Amount=' + zapper.amount +
            //    '&AmountType=' + zapper.amountType + '&MerchantReference=' + zapper.merchantReference + '&ShortMerchantName=' + zapper.shortMerchantName +
            //    '&CurrenyIsoCode =' + zapper.currenyIsoCode + '');
            return dataService.post('api/payment/getZapper', zapper)
        }
        svc.getBranchType = function (refresh) {
            return dataService.getRecord('api/payment/getbranch'.php, refresh)
        }

        svc.getTitle = function (refresh) {
            return dataService.getRecord('api/payment/gettitle.php', refresh)
        }

        svc.getRewardPoint = function (refresh) {
            return dataService.getRecord('api/payment/getRewardPoint.php', refresh)
        }
        
        svc.getFrequencyType = function (refresh) {
            return dataService.getRecord('api/payment/getfrequency.php', refresh)
        }

        svc.getCollectionType = function (refresh) {
            return dataService.getRecord('api/payment/getcollection.php', refresh)
        }
        svc.getIdentificationType = function (refresh) {
            return dataService.getRecord('api/payment/getidentification.php', refresh)
        }

        svc.getAccountType = function (refresh) {
            return dataService.getRecord('api/payment/getAccounttype.php', refresh)
        }


        svc.getCapitalAmtDetails = function (id) {
            return dataService.getRecord('api/matter/GetSelectedCapital.php/' + id)
        }
        //svc.easyDebitPayment = function (data) {
        //    return dataService.post('api/ServiceController/EasyDebitPayment', data);
        //}

        svc.easyDebitPayment = function (data) {
            return dataService.post('api/ServiceController/ClientAVSSubmissionRequest', data);
            }

        svc.changeGlobalDateFormat = function (date) {
            if (date != undefined) {
                var dateFormat;
                var splittedDate = date.split("/");
                return dateFormat = splittedDate[1] + '-' + splittedDate[0] + '-' + splittedDate[2];
            }
        }

        svc.getDisabledDateList = function (refresh) {
            return dataService.getRecord('api/payment/getDisabledDateList.php', refresh)
        }

        return svc;
    }

})();