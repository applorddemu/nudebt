﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('EnumService', service);

    function service() {

        var svc = {};

        //next step is to get this from the server so that it is automatically in sync with the server-side equivalent enum
        svc.permissions = {
            None: 0,
            CanAccessSettingsCompanyProfile: 1 << 0,
            CanAccessSettingsSetup: 1 << 1,
            CanAccessSettingsSystemAccess: 1 << 2,
            CanAccessContactsCompany: 1 << 3,
            CanAccessContactsIndividual: 1 << 4,
            CanAccessCommunicationSocialMedia: 1 << 5,
            CanAccessCommunicationSendSms: 1 << 6,
            CanAccessStaffStaffDetails: 1 << 7,
            CanAccessStaffSalarySchedule: 1 << 8,
            CanAccessStaffPayslip: 1 << 9,
            CanAccessFinanceQuotes: 1 << 10,
            CanAccessFinanceInvoicing: 1 << 11,
            CanAccessFinanceBusinessCash: 1 << 12,
            CanAccessFinanceBankAccounts: 1 << 13,
            CanAccessFinanceOwnersMoney: 1 << 14,
            CanAccessFinanceBusinessLoans: 1 << 15,
            CanAccessReportsWhoOwesUs: 1 << 16,
            CanAccessReportsWhoWeOwe: 1 << 17,
            CanAccessReportsCashGauge: 1 << 18,
            CanAccessReportsWhoOwesUsCustomer: 1 << 19,
            CanAccessReportsBankStatements: 1 << 20,
            CanAccessReportsVat: 1 << 21,
            CanAccessAccountantGeneralLedger: 1 << 22,
            CanAccessAccountantIncomeStatement: 1 << 23,
            CanAccessAccountantTrailBalance: 1 << 24,
            CanAccessAccountantBalanceSheet: 1 << 25,
            CanAccessAccountantCustomLedgerAccounts: 1 << 26,
            CanAccessAccountantExportData: 1 << 27,
            CanAccessAccountantCashFlow: 1 << 28,
            CanAccessFinanceInvoiceOut: 1 << 29,
            CanAccessFinanceInvoiceIn: 1 << 30,
            CanAccessFinanceCreditNote: 1 << 31,
            CanAccessAccountantAdjustments: 1 << 32,
            CanAccessSettingsImportContactsCsv: 1 << 33,
            CanAccessAccountantAskMyAccountant: 1 << 34
        };

        svc.cashFlowType = {
            MoneyIn: 1,
            MoneyOut: 2
        };


        svc.DebitRunDateType = {
            First: 1,
            Last: 2,
        };

        svc.CountryType = {
            SouthAfrica: 1,
        };

        svc.RoleType = {
            SuperAdmin: 1,
            Admin: 2,
            User: 3,
        }

        svc.NavigationType = {
            URLNavigation: 1,
            StateNavigation: 2
        }

        svc.Frequency = {
            OnceOff: 1,
            Monthly: 2,
            Weekly: 3,
            FortNightly: 4
        }
        svc.TitleType =
        {
            Mr: 1,

            Mrs: 2,

            Miss: 3,

            Dr: 4,

            Sir: 5
        }

        svc.RewardPointDescType =
        {
            EmailUpdated: 1,

            CellNoUpdated: 2,

            BankDetailsUpdated: 3,

            AdditionalInfoUpdated: 4
        }

        svc.ProfileStatusType =
        {
            IRON: 1,

            BRONZE: 2,

            GOLD: 3,

            PLATINUM: 4
        }

        svc.PreferableContactMethod =
       {
           Call: 1,

           SMS: 2,

           Email: 3,

           Whatsapp: 4
       }
        return svc;
    }

})();
