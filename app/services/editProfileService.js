﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('editProfileService', service);

    service.$inject = ['dataService', 'environment', '$localStorage'];

    function service(dataService, environment, $localStorage) {

        var svc = {};
        //Page load getdetails
        svc.getProfileDetails = function (id) {
            return dataService.getRecord('api/editprofile/GetDetails.php/'+ id)
        }
        
        svc.GetRewardStatusImageBar = function (profile) {
            return dataService.post('api/editprofile/GetRewardStatusImageBar', profile)
        }

        //Save Profile
        svc.save = function (profile) {
            return dataService.post('api/editprofile/saveProfile', profile)
        }

       // verifyRewardPts
        svc.verifyRewardPts = function (verifyRewardPts) {
            return dataService.post('api/editprofile/verifyRewardPts', verifyRewardPts)
        }
        //Save RewardPts
        svc.saveRewardPts = function (profile) {
            return dataService.post('api/editprofile/saveRewardPts', profile)
        }
        //verify account
        svc.verifyBankAccount = function (profile) {
            return dataService.post('api/ServiceController/ClientAVSSubmissionRequest', profile);
        }

        //Email Verify
        svc.verifyEmail = function (profile) {
              return dataService.post('api/ServiceController/sendContact', profile)            
        }

        //OTP Check
        svc.otpCheck = function (profile) {
            return dataService.post('api/editprofile/getNotificationOTP', profile)
        }
        
        
          //SMS Verify
        svc.verifySms = function (profile) {
            return dataService.post('api/ServiceController/sendSms', profile)
        }

        //Email Link Verification
        svc.verifyEmailLink = function (profile) {
            return dataService.post('api/ServiceController/emailLinkVerification', profile)
        }
        //TotalRewardPopints
        svc.getTotalRewardPoints = function (id) {
            return dataService.getRecord('api/editprofile/getTotalRewardPoints.php/' + id)
        }
        //TotalRewardPopint
        svc.getTotalRewardPoint = function (id) {
            return dataService.getRecord('api/editprofile/getTotalRewardPoint.php/' + id)
        }
        svc.getStatus = function (id) {
            return dataService.getRecord('api/editprofile/getStatus.php/' + id)
        }
        svc.getPreferableContactMethod = function (refresh) {
            return dataService.getLookupData('/api/editprofile/getPreferableContactMethod', refresh);
        }
        return svc;
    }

})();