﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('breakdownService', service);

    service.$inject = ['dataService', 'environment', '$localStorage'];

    function service(dataService, environment, $localStorage) {

        var svc = {};
        svc.getBreakdownList = function (matterId, matterIndexId) {
            return dataService.getRecord('api/matter/GetBreakdownDetail?matterNumber=' + matterId + "&matterIndexId=" + matterIndexId)
        }
        svc.getPreviousPaymentChartDetail = function (matterId, matterIndexId) {
            return dataService.getRecord('api/payment/GetPreviousPaymentChartDetail?matterNumber=' + matterId + "&matterIndexId=" + matterIndexId)
        }
        svc.getPreviousPaymentList = function (matterId, matterIndexId) {
            return dataService.getRecord('api/payment/GetPreviousPaymentDetail?matterNumber=' + matterId + "&matterIndexId=" + matterIndexId)
        }
        svc.getCapitalAmtDetails = function (id) {
            return dataService.getRecord('api/matter/GetSelectedCapital/' + id)
        }
        return svc;
    }

})();