(function() {
    'use strict';

    angular
        .module('app')
        .factory('navigation', service);

    service.$inject = ['$state'];

    function service($state) {

        var nav = {};

        nav.goToParentState = function () { $state.go('^'); };

        nav.goToPreviousState = function () { $state.go($state.previousState); };

        nav.goToDashboard = function () { $state.go('dashboard'); };

        nav.goToPayment = function (id) { $state.go('dashboard.payment', { id: id }); };

        nav.goToBreakDown = function (id) { $state.go('dashboard.breakdown', { id: id }); };


        //nav.goToManageTickets = function () { $state.go('dashboard.managetickets'); };

        nav.goToNotifymeServiceMessage = function (referenceNo) { $state.go('dashboard.servicemessage', { referenceNumber: referenceNo }); };

        nav.goToLogin = function () { $state.go('login'); };

        nav.goToLink = function () { $state.go('dashboard.link'); };
       
        nav.goToBee = function () { $state.go('dashboard.bee'); };

        nav.goToThankYou = function () { $state.go('dashboard.thankYou'); };
        //nav.goToRegister = function () { $state.go('login.register'); };

        //nav.goToaccessdenied = function () { $state.go('dashboard.accessdenied'); };

        nav.goToPaymentCompleted = function () { $state.go('dashboard.paymentMessage'); };

        nav.goToProfile = function () { $state.go('dashboard.editprofile'); };

        nav.goToPaymentRedirect = function (data) { $state.go('dashboard.paymentProcess', { data: data }); };

        nav.isOnLoginOrRegisterScreen = function () {
            return $state.is('login') || $state.is('register') || $state.is('home');
        };
         
        return nav;
    }

})();
