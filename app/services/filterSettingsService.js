﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('FilterSettingsService', viewmodel);

    viewmodel.$inject = ['dateService'];
    /* @ngInject */
    function viewmodel(dateService) {

        var filterSettings = function() {
            this.search = null;
            this.from = dateService.getDefaultFromDate();
            this.to = dateService.getDefaultToDate();
        };

        var vm = {
            newregistrations: new filterSettings(),
            audits: new filterSettings(),
            logs: new filterSettings()
        };

        return vm;
    }

})();