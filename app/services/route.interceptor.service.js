﻿(function () {
    'use strict';
    angular
        .module('app')
		.config(routeInterceptor)

    routeInterceptor.$inject = ['$urlRouterProvider', 'environment'];

    function routeInterceptor($urlRouterProvider, environment) {
    	var virtualPathPrefix = environment.virtualPathPrefix;


    	$urlRouterProvider.rule(function ($injector, $location) {

    		//what this function returns will be set as the $location.url
    		var path = $location.path();

    		if (virtualPathPrefix != '') {
    			if (path.indexOf(virtualPathPrefix)==-1) {
    				var virtualPath = virtualPathPrefix + path;

    				$location.replace().path(virtualPath);
    			}

    			return $location.path();
    		}

    		//, normalized = path.toLowerCase();
    		//if (path != normalized) {
    		//	//instead of returning a new url string, I'll just change the $location.path directly so I don't have to worry about constructing a new url string and so a new state change is not triggered
    		//	$location.replace().path(normalized);
    		//}
    		// because we've returned nothing, no state change occurs
    	});
    }
})();