﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('homeService', service);

    service.$inject = ['dataService', 'environment','$localStorage'];

    function service(dataService, environment, $localStorage) {

        var svc = {};

        angular.isUndefinedOrNull = function (val) {
            return angular.isUndefined(val) || val === null || val === "";
        }

        svc.getApplicationsAndRequest = function (MatterNumer) {
            if (!angular.isUndefinedOrNull($localStorage.userDetails) && !angular.isUndefinedOrNull($localStorage.userDetails.C_DebtorIdentityNo))
                return dataService.getRecord('api/matter/GetMatterListByMatterId/' + $localStorage.userDetails.C_DebtorIdentityNo)
        }
        svc.getCurrentDebtor = function () {
            if (!angular.isUndefinedOrNull($localStorage.userDetails) && !angular.isUndefinedOrNull($localStorage.userDetails.C_DebtorIdentityNo))
                return dataService.getRecord('api/debtor/getCurrentDebtor/' + $localStorage.userDetails.C_DebtorIdentityNo)
        }
        svc.getAppDetails = function (MatterNumer) {
            if (!angular.isUndefinedOrNull($localStorage.userDetails) && !angular.isUndefinedOrNull($localStorage.userDetails.C_DebtorIdentityNo))
                return dataService.getRecord('api/matter/getPaymentListByClientId/' + $localStorage.userDetails.C_DebtorIdentityNo)
        }

        svc.getMatterCountByClientId = function () {
            if (!angular.isUndefinedOrNull($localStorage.userDetails) && !angular.isUndefinedOrNull($localStorage.userDetails.C_DebtorIdentityNo))
                return dataService.getRecord('api/matter/GetMatterCountByClientId/' + $localStorage.userDetails.C_DebtorIdentityNo)
        }

        return svc;
    }

})();