﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('notificationService', service);

    service.$inject = ['dataService', 'environment', '$localStorage'];

    function service(dataService, environment, $localStorage) {

        var svc = {};
        svc.getNotification = function () {
            return dataService.getRecord('api/notification/getnotification.php')
        }

        svc.sendEmail = function (contact) {
            return dataService.post('api/notification/sendContact',contact)

        }
        svc.genotificationSearchList = function (searchQuery) {
            return dataService.getRecordWithParams('api/notification/getnotificationSearchList.php', searchQuery);
        }
        return svc;
    }

})();