(function () {
    'use strict';

    angular
        .module('app')
        .service('session', session); 

    session.$inject = ['localStorageService', 'environment'];

    function session(localStorageService, env) {

        var localStorageSessionKey = 'NuDebtCollect-' + env.environment + '-AuthData';

        this.create = function (accessToken) {// jshint ignore:line
            var decodedToken = jwt_decode(accessToken);// jshint ignore:line
            var userId = decodedToken['user.id'];
            var userFirstName = decodedToken['user.firstname'];
            var username = decodedToken['user.username'];
            var userLastName = decodedToken['user.lastname'];
            var email = decodedToken['user.emailid'];
            var identificationnumber = decodedToken['user.identificationnumber'];
            var landlinenumber = decodedToken['user.landlinenumber'];
            var mobilenumber = decodedToken['user.mobilenumber'];
            var primarycontactnumber = decodedToken['user.primarycontactnumber'];
            var title = decodedToken['user.title'];
            var roletype = decodedToken['user.roletype'];
            var roleId = decodedToken['user.roleId'];
            var userProfileStream = decodedToken['user.userProfileStream'];
            var isSystemGenerated = decodedToken['user.isSystemGenerated'];


            this.setLocalStorageProperties(userId, userFirstName, username, userLastName, email, identificationnumber, landlinenumber, mobilenumber, primarycontactnumber, title, roletype, roleId, accessToken, userProfileStream, isSystemGenerated);
            this.setSessionProperties(userId, userFirstName, username, userLastName, email, identificationnumber, landlinenumber, mobilenumber, primarycontactnumber, title, roletype, roleId, accessToken, userProfileStream, isSystemGenerated);
        };

        this.update = function (updateDetails) {// jshint ignore:line
            var localData = localStorageService.get(localStorageSessionKey);
            var accessToken = localData.accessToken;
            var userId = localData.userId;
            var userFirstName = updateDetails.firstName;
            var username = updateDetails.fullName;
            var userLastName = updateDetails.lastName;
            var email = localData.emailId;
            var identificationnumber = localData.identificationNumber;
            var landlinenumber = updateDetails.landLineNumber;
            var mobilenumber = updateDetails.mobileNumber;
            var primarycontactnumber = updateDetails.primaryContactNumber;
            var title = localData.tiTle;
            var roletype = localData.roletype;
            var roleId = localData.roleId;
            var userProfileStream = updateDetails.userProfileStream;
            var isSystemGenerated = decodedToken['user.isSystemGenerated'];


            this.setLocalStorageProperties(userId, userFirstName, username, userLastName, email, identificationnumber, landlinenumber, mobilenumber, primarycontactnumber, title, roletype, roleId, accessToken, userProfileStream, isSystemGenerated);
            this.setSessionProperties(userId, userFirstName, username, userLastName, email, identificationnumber, landlinenumber, mobilenumber, primarycontactnumber, title, roletype, roleId, accessToken, userProfileStream, isSystemGenerated);
        };

        this.destroy = function () {// jshint ignore:line
            this.setLocalStorageProperties();
            this.setSessionProperties();
        };

        this.load = function () {// jshint ignore:line
            var localData = localStorageService.get(localStorageSessionKey);
            if (localData) {
                this.setSessionProperties(localData.userId, localData.userFirstName, localData.username, localData.userLastName, localData.emailId, localData.identificationNumber, localData.landlineNumber, localData.mobileNumber, localData.primaryContactNumber, localData.tiTle, localData.roletype, localData.roleId, localData.accessToken, localData.userProfileStream, localData.isSystemGenerated);
            }
        };

        this.setSessionProperties = function (userid, userfirstname, username, userLastName, emailId, identificationNumber, landlineNumber, mobileNumber, primaryContactNumber, tiTle, roletype, roleId, accessToken, userProfileStream, isSystemGenerated) { // jshint ignore:line
            this.userId = userid,
            this.userFirstName = userfirstname,
            this.userLastName = userLastName,
            this.emailId = emailId,
            this.username = username;
            this.accessToken = accessToken;
            this.identificationNumber = identificationNumber,
            this.landlineNumber = landlineNumber,
            this.mobileNumber = mobileNumber,
            this.primaryContactNumber = primaryContactNumber;
            this.tiTle = tiTle;
            this.roletype = roletype;
            this.roleId = roleId;
            this.isSystemGenerated = isSystemGenerated;
        };

        this.setLocalStorageProperties = function (userid, userfirstname, username, userlastname, email, identificationnumber, landlinenumber, mobilenumber, primarycontactnumber, title, roletype, roleId, accessToken, userProfileStream, isSystemGenerated) {// jshint ignore:line
            localStorageService.set(localStorageSessionKey, {
                userId: userid,
                userFirstName: userfirstname,
                userLastName: userlastname,
                emailId: email,
                identificationNumber: identificationnumber,
                landlineNumber: landlinenumber,
                mobileNumber: mobilenumber,
                primaryContactNumber: primarycontactnumber,
                tiTle: title,
                accessToken: accessToken, // jshint ignore:line
                username: username,
                roletype: roletype,
                roleId: roleId,
                userProfileStream: userProfileStream,
                isSystemGenerated: isSystemGenerated,
            });
        };
    }

})();
