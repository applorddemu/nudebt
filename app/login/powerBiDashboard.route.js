﻿
(function () {
	'use strict';

	angular
        .module('app.login')
        .run(appRun);

	appRun.$inject = ['routerHelper'];
	/* @ngInject */
	function appRun(routerHelper) {
		routerHelper.configureStates(getStates());
	}

	function getStates() {
		return [
            {
            	state: 'dashboard.powerBiDashboard',
            	config: {
            		url: 'powerBiDashboard',
            		templateUrl: '/app/login/powerBiDashboard.html',
            		controller: 'PowerBiDashboardController',
            		controllerAs: 'vm',
            		title: 'powerBiDashboard',
            		settings: {
            			mustBeAuthenticated: false
            		},
            	}
            }
		];
	}
})();
