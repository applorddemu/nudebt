﻿
(function () {
    'use strict';

    angular
        .module('app.login')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.bee',
                config: {
                    url: 'bee',
                    templateUrl: '/app/login/bee.html',
                    controller: 'BeeController',
                    controllerAs: 'vm',
                    title: 'Bee',
                    settings: {
                        mustBeAuthenticated: false
                    },
                }
            }
        ];
    }
})();
