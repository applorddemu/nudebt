﻿
(function () {
    'use strict';

    angular
        .module('app.login')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.link',
                config: {
                    url: '/link',
                    templateUrl: '/app/login/link.html',
                    controller: 'LinkController',
                    controllerAs: 'vm',
                    title: 'Link',
                    settings: {
                        mustBeAuthenticated: false
                    },
                }
            }
        ];
    }
})();
