(function () {
    'use strict';

    angular
        .module('app.login')
        .controller('LoginController', controller);

    controller.$inject = ['authService', 'navigation', '$q', 'messagingService', 'log', 'activeUserSession', 'modalService', 'session', 'EnumService', '$http', '$rootScope', '$localStorage', 'notificationService'];
    /* @ngInject */
    function controller(authService, navigation, $q, messagingService, log, activeUserSession, modalService, session, EnumService, $http, $rootScope, $localStorage, notificationService) {
        var vm = this;// jshint ignore:line
        vm.isLoadinglogin = false;
        $localStorage.isAuthenticationlogin = false;
        $localStorage.isAdmin = false;
        vm.isCheck = true;
        vm.isCellNumerChecked = true; 
        var imagePath = "images/edit_profile/u157_" + $localStorage.ProfileStatusType + ".png";
        $localStorage.statusImage = imagePath;
        vm.credentials = {
            matternumber: '',
            cellnumber: '',
            idnumber: ''
        };
        vm.radioCellNoChange = function (e) {
            vm.credentials.cellnumber = '';
            vm.credentials.idnumber = '';
            if (e == 1) {
                vm.isCellNumerChecked = true;
                vm.isCheck = true;
            }
            else {
                vm.isCellNumerChecked = false;
                vm.isCheck = false;
            }


        }
        $rootScope.layoutHeaderChanges = true;

        vm.login = function (credentials, form) {
            if (credentials.matternumber == null || credentials.matternumber == "" || credentials.matternumber == undefined) {
                log.error("Error", "Matter Number should not be Empty");
                return;
            }
            if (vm.isCellNumerChecked == true && (credentials.cellnumber == null || credentials.cellnumber == "" || credentials.cellnumber == undefined)) {
                log.error("Error", "Cell Number should not be Empty");
                return;
            }
            if (vm.isCellNumerChecked == false && (credentials.idnumber == null || credentials.idnumber == "" || credentials.idnumber == undefined)) {
                log.error("Error", "ID Number should not be Empty");
                return;
            }
            messagingService.broadcastCheckFormValidatity();
            if (!form.$invalid) {
                vm.isLoadinglogin = true;
                if (credentials.matternumber == 9 && credentials.cellnumber == 9999999999) {
                    $localStorage.isAdmin = true;
                    navigation.goToLink();
                }
                else {
                    authService.login(credentials).then(function (res) {
                       
                        if (!angular.isUndefined(res)) {
                            $localStorage.userDetails = res;
                            $rootScope.layoutHeaderChanges = false;
                            $localStorage.isAuthenticationlogin = true;
                            $localStorage.DebtorName = res.D_FullNames;
                            $rootScope.DebtorName = res.D_FullNames;
                            $localStorage.ProfileStatusId = res.ProfileStatusType;
                            $localStorage.ProfileStatusTypeName = res.ProfileStatusTypeName;
                           // $localStorage.statusImage = imagePath;
                            vm.isLoadinglogin = false;
                            navigation.goToDashboard();
                        }

                    }, function (err) {
                        vm.isLoadinglogin = false;
                    });
                }
            }
        };

        vm.forgetPassword = function () {

            modalService.forgotpassword();
        };

        $('.btn-header, .ccc-icon, .goLink').on("click", function (e) {
            e.preventDefault();

            $("body, html").animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, 600);

        });

        angular.isUndefinedOrNull = function (val) {
            return angular.isUndefined(val) || val === null;
        }      

        vm.sendEmail = function (notificationForm, notification) {
            if (angular.isUndefinedOrNull(notification)) {
                log.error("Error", "Contact Details should not be Empty");
                return;
            }
            if (angular.isUndefinedOrNull(notification.Name)) {
                log.error("Error", "Name should not be Empty");
                return;
            }
            if (angular.isUndefinedOrNull(notification.Recipient)) {
                log.error("Error", "Email should not be Empty");
                return;
            }
            if (angular.isUndefinedOrNull(notification.Telephone)) {
                log.error("Error", "Mobile Number should not be Empty");
                return;
            }
            //else if (notification.Telephone.toString().length < 10) {
            //    log.error("Error", "Invalid Mobile Number");
            //    return;
            //}
            if (!notificationForm.$invalid) {
                notificationService.sendEmail(notification).then(function (response) {
                    //  swal("Success!", "Message Sent Successfully!", "success");
                    vm.notification = null;
                    navigation.goToThankYou();                   
                },
                function (err) {
                });
            }
        }


        vm.beeClick = function () {
            navigation.goToBee();
        }

        activate();

        function activate() { }

        var links = $rootScope.link;
        if (!angular.isUndefinedOrNull(links)) {
            $("body, html").animate({
                scrollTop: $("#" + links).offset().top
            }, 600);
            $rootScope.link = null;
        }

        return vm;

    }
})();
