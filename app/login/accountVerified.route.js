﻿
(function () {
    'use strict';

    angular
        .module('app.login')
        .run(appRun);
    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.accountVerified',
                config: {
                    url: 'accountVerified?myParam',
                    templateUrl: '/app/login/accountVerified.html',
                    controller: 'AccountVerifiedController',
                    controllerAs: 'vm',
                    title: 'accountVerified',
                    settings: {
                        mustBeAuthenticated: false
                    },
                }
            }
        ];
    }
})();
