﻿
(function () {
    'use strict';

    angular
        .module('app.login')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.thankYou',
                config: {
                    url: 'thankYou',
                    templateUrl: '/app/login/thankYou.html',
                    controller: 'ThankYouController',
                    controllerAs: 'vm',
                    title: 'thankYou',
                    settings: {
                        mustBeAuthenticated: false
                    },
                }
            }
        ];
    }
})();
