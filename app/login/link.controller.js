﻿(function () {
    'use strict';

    angular
        .module('app.login')
        .controller('LinkController', controller);

    controller.$inject = ['authService','navigation','$http', '$rootScope', '$localStorage'];
    function controller(authService, navigation, $http, $rootScope, $localStorage) {
        var vm = this;
        $rootScope.layoutHeaderChanges = null;
        $rootScope.header = "Links";
        return vm;
    }
})();
