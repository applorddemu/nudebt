﻿(function () {
    //'use strict';

    angular
        .module('app.login')
        .controller('AccountVerifiedController', controller);

    controller.$inject = ['authService', 'navigation', '$q', '$rootScope', 'editProfileService', '$localStorage', '$scope'];
    /* @ngInject */
    function controller(authService, navigation, $q, $rootScope, editProfileService, $localStorage, $scope) {
        var vm = this;// jshint ignore:line
        $rootScope.layoutHeaderChanges = null;
        $rootScope.header = null;
       
        $scope.profileEmailVerified = {
            emailVerified: false,
            profileInfoEmail: ''
        };

        angular.isUndefinedOrNull = function (val) {
            return angular.isUndefined(val) || val === null || val === "";
        }

        vm.click = function (links) {
            $rootScope.link = links;
            navigation.goToLogin();
        };

        var QueryStringArray = window.location.hash.substr(1).split('&');
        var encodeString = QueryStringArray[0].replace('/accountVerified?','');
        var emailLinkVerification = {};
        emailLinkVerification.EncryptedKey = encodeString.replace('%3D','=');

        editProfileService.verifyEmailLink(emailLinkVerification).then(function (response) {
            if (response != null && response.Result) {
                if (!angular.isUndefinedOrNull($localStorage.userDetails)
                    && !angular.isUndefinedOrNull($localStorage.userDetails.C_MatterID)
                    && ($localStorage.userDetails.C_MatterID == response.Result.MatterId
                    || $localStorage.userDetails.C_MatterID == response.Result.Matter_Id)) {
                    //$rootScope.emailVerified = true;
                    //$rootScope.profileInfoEmail = response.Result.Email;
                    $scope.profileEmailVerified.emailVerified = true;
                    $scope.profileEmailVerified.profileInfoEmail = response.Result.Email;
                    $rootScope.$broadcast("Update", $scope.profileEmailVerified);
                    //$scope.$broadcast("Update", $scope.profileEmailVerified);
                    // firing an event upwards
                    //$scope.$emit('Update', $scope.profileEmailVerified);
                    signalrListener();
                }



                function signalrListener()
                {
                    try {
                        debugger;
                        //$.connection.hub.qs = { "userId": $localStorage.userDetails.C_MatterID };
                        $.connection.hub.url = config.chatUrl;
                        var chat = $.connection.publicHub;    

                        $.connection.hub.start().done(function () {
                            console.log('Now connected, connection ID=' + $.connection.hub.id);
                            chat.server.sendEmailVerified($localStorage.userDetails.C_MatterID);
                        });
                    } catch (e) {

                    }
                };



            }
        });

        return vm;

    }
})();
