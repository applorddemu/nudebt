﻿(function () {
    'use strict';

    angular
        .module('app.login')
        .controller('ActivateUserController', controller);

    controller.$inject = ['navigation', 'authService', 'userDataService', 'QueryString'];
    /* @ngInject */
    function controller(navigation, authService, userDataService, QueryString) {
        var vm = this;// jshint ignore:line

        vm.isSuccess = false;
        function activate() {
            vm.activate();
        }


        vm.activate = function () {
            userDataService.activateUser(QueryString).then(function (res) {
               vm.isActivated=true
            });           
        }



        activate();

        return vm;
    }
})();
