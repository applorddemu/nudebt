﻿
(function () {
    'use strict';

    angular
        .module('app.login')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'ActivateUser',
                config: {
                    url: '/confirmUser/{QueryString}',
                    templateUrl: '/app/login/activateuser.html',
                    controller: 'ActivateUserController',
                    controllerAs: 'vm',
                    title: 'ActivateUser',
                    settings: {
                        mustBeAuthenticated: false
                    },
                    resolve: {
                        QueryString: ['$stateParams', function ($stateParams) {
                            return $stateParams.QueryString;
                        }],
                    },

                }
            }
        ];
    }
})();
