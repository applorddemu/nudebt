﻿(function () {
    'use strict';

    angular
        .module('app.login')
        .controller('PowerBiDashboardController', controller);

    controller.$inject = ['authService', 'navigation', '$q', '$rootScope'];
    /* @ngInject */
    function controller(authService, navigation, $q, $rootScope) {
        var vm = this;// jshint ignore:line
        $rootScope.layoutHeaderChanges = null;
        $rootScope.header = null;
        vm.click = function (links) {
            $rootScope.link = links;
            navigation.goToLogin();
        }
        return vm;

    }
})();
