angular.module("app")

.constant("environment", {
    "serverBaseUrl": "https://nudebt.applordtest.co.za/WebApi/",
    "clientBaseUrl": "https://nudebt.applordtest.co.za/",
	"environment": "LOCAL",
	"version": "1.0.0.0",
	"virtualPathPrefix": "", //Note : To host client and Api in same site in IIS - Virtual path name
})


.constant("applicationSettings", {
    "defaultDateFormat": "DD MMM YYYY hh:mm a",
"PreferableContactMethod" : {
Call: "Call",
SMS: "SMS",
Email: "Email",
Whatsapp:"Whatsapp"
},
"ProfileStatusType": {
    IRON: "IRON",
    BRONZE: "BRONZE",
    GOLD: "GOLD",
    PLATINUM: "PLATINUM"
},
"ProfileStatus": "YOU'VE REACHED PLATINUM",


});

//var virtualPath = "/Client";
var virtualPath = "";
var webAPIURL = "http://192.168.0.31:8058/WebApi";
var clientUrl = "http://192.168.0.31:8057" + virtualPath;

var config = {
    virtualPath: virtualPath,
    pageSize: 10,
    appServicesHostName: webAPIURL,
    clientUrl: clientUrl,
    dateFormat: "dd-MM-yyyy",
    dateFormatWithTime: "dd-MM-yyyy hh:mm tt",
    dateFormatWithTime24Hours: "dd-MM-yyyy HH:mm",
    chatUrl: "https://51.140.42.170:8088/signalr"
};
