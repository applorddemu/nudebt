(function () {
    'use strict';

    angular.module('app')
        .run(['session', function (session) {
            session.load();
        }]);

})();
